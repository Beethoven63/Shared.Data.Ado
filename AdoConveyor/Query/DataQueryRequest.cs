using Shared.Data.Ado.AdoConveyor.Change;
using Shared.Data.Ado.Paging;
using System;

namespace Shared.Data.Ado.AdoConveyor.Query
{
    //TODO_JOE:
    //Now this is named IDataChangeRequest, suggesting it is just for queries, but the IQueryRequest has been disabled,
    //so this is used for all operations.  Need to change name.
    //Note that this is just for databases, whereas the interface is for all data sources
    /// <summary></summary>
    /// <typeparam name="TParam">The type of the parameter.</typeparam>
    /// <typeparam name="TData">The type of the data.</typeparam>
    public class DataQueryRequest<TParam, TData>: IDataChangeRequest<TParam, TData>
    {
        #region Properties

        /// <summary>Gets or sets the connection key.</summary>
        /// <value>The connection key.</value>
        public string ConnectionKey{get; set;}

        /// <summary>Gets or sets the data.</summary>
        /// <value>The data.</value>
        public TData Data{get; set;}

        /// <summary>Gets or sets the name.</summary>
        /// <value>The name.</value>
        public string Name{get; set;}

        /// <summary>Gets or sets the page.</summary>
        /// <value>The page.</value>
        public PageParam Page{get; set;}

        /// <summary>Gets or sets the parameter.</summary>
        /// <value>The parameter.</value>
        public TParam Param{get; set;}

        /// <summary>Gets the query procedure.</summary>
        /// <value>The query procedure.</value>
        public IStoredProcedure<TParam, TData> QueryProcedure{get; internal set;}

        /// <summary>Gets or sets the type.</summary>
        /// <value>The type.</value>
        public Type Type{get; set;}

        #endregion
    }
}