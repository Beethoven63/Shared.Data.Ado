namespace Shared.Data.Ado.AdoConveyor.Query
{
    /// <summary></summary>
    public enum AdoOperation
    {
        /// <summary>The delete</summary>
        Delete,

        /// <summary>The get item</summary>
        GetItem,

        /// <summary>The get list</summary>
        GetList,

        /// <summary>The save</summary>
        Save
    }
}