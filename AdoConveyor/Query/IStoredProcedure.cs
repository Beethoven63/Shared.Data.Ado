using Shared.Data.Ado.AdoConveyor.Change;
using Shared.Data.Ado.DataConfiguration;
using Shared.Data.Ado.Parameter;
using Shared.Logging.Responses;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Shared.Data.Ado.AdoConveyor.Query
{
    /// <summary></summary>
    /// <typeparam name="TParam">The type of the parameter.</typeparam>
    /// <typeparam name="TData">The type of the data.</typeparam>
    public interface IStoredProcedure<TParam, TData>
    {
        #region Properties

        /// <summary>Gets or sets the address.</summary>
        /// <value>The address.</value>
        DataObjectAddress Address{get; set;}

        /// <summary>Gets or sets the connection key.</summary>
        /// <value>The connection key.</value>
        string ConnectionKey{get; set;}

        /// <summary>Gets or sets the operation.</summary>
        /// <value>The operation.</value>
        AdoOperation Operation{get; set;}

        /// <summary>Gets or sets the parameter.</summary>
        /// <value>The parameter.</value>
        object Param{get; set;}

        /// <summary>Gets or sets the parameters.</summary>
        /// <value>The parameters.</value>
        List<SqlParameter> Parameters{get; set;}

        /// <summary>Gets or sets the parameter object.</summary>
        /// <value>The parameter object.</value>
        TParam ParamObject{get; set;}

        #endregion

        #region Methods

        /// <summary>Defines the command.</summary>
        /// <param name="con">The con.</param>
        /// <param name="inputParam">The input parameter.</param>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        SqlCommand DefineCommand(SqlConnection con, TParam inputParam, List<IParameterSource> data);

        /// <summary>Executes the change.</summary>
        /// <returns></returns>
        ChangeResponse ExecuteChange();

        /// <summary>Executes the query.</summary>
        /// <param name="inputParam">The input parameter.</param>
        /// <returns></returns>
        BaseResponse<List<TData>> ExecuteQuery(TParam inputParam);

        /// <summary>From the data row.</summary>
        /// <param name="row">The row.</param>
        /// <returns></returns>
        TData FromDataRow(DataRow row);

        /// <summary>Determines whether [is parameter valid].</summary>
        /// <returns><c>true</c> if [is parameter valid]; otherwise, <c>false</c> .</returns>
        bool IsParamValid();

        /// <summary>Reads the scopy identity.</summary>
        /// <param name="scopeIdentity">The scope identity.</param>
        void ReadScopeIdentity(int scopeIdentity);

        /// <summary>Rows to object.</summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        TData RowToObject(DataRow data);

        #endregion
    }
}