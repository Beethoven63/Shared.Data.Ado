using Shared.Data.Ado.AdoConveyor.Query;
using Shared.Data.Ado.Paging;
using System;

namespace Shared.Data.Ado.AdoConveyor.Change
{
    /// <summary></summary>
    /// <typeparam name="TParam">The type of the parameter.</typeparam>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    public class DataChangeRequest<TParam, TResult>: IDataChangeRequest<TParam, TResult>
            where TResult: new()
    {
        #region Properties

        /// <summary>Gets or sets the change procedure.</summary>
        /// <value>The change procedure.</value>
        public IStoredProcedure<TParam, TResult> ChangeProcedure{get; set;}

        /// <summary>Gets or sets the connection key.</summary>
        /// <value>The connection key.</value>
        public string ConnectionKey{get; set;}

        /// <summary>Gets or sets the data.</summary>
        /// <value>The data.</value>
        public TResult Data{get; set;}

        /// <summary>Gets or sets the name.</summary>
        /// <value>The name.</value>
        public string Name{get; set;}

        /// <summary>Gets or sets the page.</summary>
        /// <value>The page.</value>
        public PageParam Page{get; set;}

        /// <summary>Gets or sets the parameter.</summary>
        /// <value>The parameter.</value>
        public TParam Param{get; set;}

        /// <summary>Gets or sets the type.</summary>
        /// <value>The type.</value>
        public Type Type{get; set;}

        #endregion
    }
}