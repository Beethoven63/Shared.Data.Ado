using Shared.Logging.Responses;

namespace Shared.Data.Ado.AdoConveyor.Change
{
    /// <summary></summary>
    public class ChangeResponse: BaseResponse<int>
    {
        #region Properties

        /// <summary>Gets or sets a value indicating whether [response returned].</summary>
        /// <value><c>true</c> if [response returned]; otherwise, <c>false</c> .</value>
        public bool ResponseReturned{get; set;}

        #endregion
    }
}