using Shared.Data.Ado.Paging;
using System;

namespace Shared.Data.Ado.AdoConveyor.Change
{
    /// <summary></summary>
    /// <typeparam name="TParam">The type of the parameter.</typeparam>
    /// <typeparam name="TData">The type of the data.</typeparam>
    public interface IDataChangeRequest<TParam, TData>
    {
        #region Properties

        /// <summary>Gets or sets the data.</summary>
        /// <value>The data.</value>
        TData Data{get; set;}

        /// <summary>Gets or sets the name.</summary>
        /// <value>The name.</value>
        string Name{get; set;}

        /// <summary>Gets or sets the page.</summary>
        /// <value>The page.</value>
        PageParam Page{get; set;}

        /// <summary>Gets or sets the parameter.</summary>
        /// <value>The parameter.</value>
        TParam Param{get; set;}

        /// <summary>Gets or sets the type.</summary>
        /// <value>The type.</value>
        Type Type{get; set;}

        #endregion
    }
}