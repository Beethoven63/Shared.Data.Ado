using Shared.Data.Ado.Paging;

namespace Shared.Data.Ado.AdoConveyor.Deprecated
{
    /// <summary></summary>
    /// <typeparam name="TParam">The type of the parameter.</typeparam>
    public interface IQueryRequest<TParam>
    {
        #region Properties

        /// <summary>Gets or sets the page.</summary>
        /// <value>The page.</value>
        PageParam Page{get; set;}

        /// <summary>Gets or sets the parameter.</summary>
        /// <value>The parameter.</value>
        TParam Param{get; set;}

        #endregion
    }
}