using Shared.Data.Ado.DataConfiguration;
using Shared.FunctionalPattern;
using Shared.Strings;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;

namespace Shared.Data.Ado.AdoConveyor.Query
{
    /// <summary></summary>
    public sealed class DatabaseObjectStringConverter:
            ConfigurationConverterBase
    {
        #region Fields

        /// <summary>The guardian</summary>
        private readonly Guardian _guardian = new Guardian();

        #endregion

        #region Methods

        /// <summary>Determines whether the conversion is allowed.</summary>
        /// <param name="ctx">The <see cref="ITypeDescriptorContext"/> object used for <paramref name="type"/> conversions.</param>
        /// <param name="type">The <see cref="Type"/> to convert from.</param>
        /// <returns><see langword="true"/> if the conversion is allowed; otherwise, <see langword="false"/> .</returns>
        public override bool CanConvertFrom(ITypeDescriptorContext ctx, Type type)
        {
            return type == typeof(string);
        }

        /// <summary>Determines whether the conversion is allowed.</summary>
        /// <param name="ctx">The <see cref="ITypeDescriptorContext"/> object used for <paramref name="type"/> conversion.</param>
        /// <param name="type">The type to convert to.</param>
        /// <returns><see langword="true"/> if the conversion is allowed; otherwise, <see langword="false"/> .</returns>
        public override bool CanConvertTo(ITypeDescriptorContext ctx, Type type)
        {
            return type == typeof(DataObjectAddress);
        }

        /// <summary>Converts from.</summary>
        /// <param name="ctx">The CTX.</param>
        /// <param name="ci">The ci.</param>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        public override object ConvertFrom(ITypeDescriptorContext ctx, CultureInfo ci, object data)
        {
            _guardian.CheckObject(data);

            var addressParts = ((string)data).Parse("|");

            return new DataObjectAddress
            {
                Type        = addressParts[index: 0].ToEnum<ObjectType>(),
                Environment = addressParts[index: 1],
                Server      = addressParts[index: 2],
                Database    = addressParts[index: 3],
                Schema      = addressParts[index: 4],
                DbObject    = addressParts[index: 5]
            };
        }

        /// <summary>Converts to.</summary>
        /// <param name="ctx">The CTX.</param>
        /// <param name="ci">The ci.</param>
        /// <param name="value">The value.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public override object ConvertTo(ITypeDescriptorContext ctx, CultureInfo ci,
                                         object value, Type type)
        {
            ValidateType(value, typeof(DataObjectAddress));

            return ((DataObjectAddress)value).ToString();
        }

        /// <summary>Validates the type.</summary>
        /// <param name="value">The value.</param>
        /// <param name="expected">The expected.</param>
        /// <returns></returns>
        internal bool ValidateType(object value,
                                   Type expected)
        {
            _guardian.CheckObject(value);

            if(value.GetType() == expected)
                return true;

            return _guardian.HandleError(() => false, $"Unable to validate value {value} to type, {expected}");
        }

        #endregion
    }
}