using Shared.Data.Ado.AdoConveyor.Change;
using Shared.Data.Ado.AdoConveyor.Conveyor;
using Shared.Data.Ado.AdoConveyor.Query;
using Shared.Data.Ado.Extensions;
using Shared.Data.Ado.Parameter;
using Shared.FunctionalPattern;
using Shared.Logging.MainLogging;
using Shared.Logging.Responses;
using Shared.Strings;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Shared.Data.Ado.AdoConveyor.Deprecated
{
    //Need to incorporate async await
    //Am I using the right transaction class?
    //Need to check that my result set is what is expected (number of tables, number of records / table)

    /// <summary></summary>
    /// <typeparam name="TParam">The type of the parameter.</typeparam>
    /// <typeparam name="TData">The type of the data.</typeparam>
    public class AdoHelper<TParam, TData>: IDataConveyor<TParam, TData>
            where TData: class, new()
    {
        #region Fields

        /// <summary>The guardian</summary>
        private readonly Guardian _guardian = new Guardian();

        /// <summary>The parameter sources</summary>
        private readonly ParameterSourceConveyor _paramSources = new ParameterSourceConveyor();

        /// <summary>The table</summary>
        private DataTable _table = new DataTable();

        #endregion

        #region Properties

        /// <summary>Gets or sets the get change procedure.</summary>
        /// <value>The get change procedure.</value>
        public Func<AdoOperation, IStoredProcedure<TParam, TData>> GetChangeProcedure{get; set;}

        /// <summary>Gets or sets the get query procedure.</summary>
        /// <value>The get query procedure.</value>
        public Func<AdoOperation, IStoredProcedure<TParam, TData>> GetQueryProcedure{get; set;}

        #endregion

        #region Methods

        #region IDataConveyor<TParam,TData> Members

        /// <summary>Deletes the data.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<TParam> DeleteData(IDataChangeRequest<TParam, TData> request)
        {
            Func<DataChangeRequest<TParam, TData>, BaseResponse<TParam>> op = MakeDatabaseChange;

            return HandleChangeRequest(request, AdoOperation.Delete, op);
        }

        /// <summary>Gets the item.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<TData> GetItem(IDataChangeRequest<TParam, TData> request)
        {
            throw new NotImplementedException();
        }

        //public BaseResponse<TData> GetItem(IDataChangeRequest<TParam, TData> request)
        //{
        //    _guardian.CheckObject(GetQueryProcedure);

        // var queryRequest = ConvertQueryToRequest(request, AdoOperation.GetItem); var response =
        // MakeDatabaseRequest(queryRequest); if (!response.Success) return new BaseResponse<TData>();

        // _guardian.CheckObject(response.Data); _guardian.CheckObject(response.Data.Tables);

        // var tables = response.Data.Tables.ToList(); if (!_guardian.CheckCollection(tables, true,
        // false) || tables.Count == 0) return new BaseResponse<TData>();

        // var list = response.Data.Tables[0].ToRowList(); if (!_guardian.CheckCollection(list,
        // true, false)) return new BaseResponse<TData>();

        // var row = list.FirstOrDefault(); if (row == null) return new BaseResponse<TData>();

        // var item = queryRequest.QueryProcedure.RowToObject(row); _guardian.CheckObject(queryRequest.QueryProcedure);

        // ClearParamSources();

        //    return new BaseResponse<TData>
        //    {
        //        Data = item,
        //        Success = item != null
        //    };
        //}

        /// <summary>Gets the list.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<List<TData>> GetList(IDataChangeRequest<TParam, TData> request)
        {
            _guardian.CheckObject(GetQueryProcedure);
            var queryRequest = ConvertQueryToRequest(request, AdoOperation.GetList);

            //Request the data from the database
            var response = MakeDatabaseRequest(queryRequest);

            if(!response.Success)
                return new BaseResponse<List<TData>>();

            //Convert data to a list of tables
            _guardian.CheckObject(response.Data);
            _guardian.CheckObject(response.Data.Tables);
            _table = response.Data.Tables.ToList().FirstOrDefault();

            var list = _table.ToObjectList(r => queryRequest.QueryProcedure.RowToObject(r));

            ClearParamSources();

            return new BaseResponse<List<TData>> {Data = list, Success = list != null};
        }

        //public BaseResponse<TParam> SaveData(IDataChangeRequest<TParam, TData> request)
        //{
        //}

        /// <summary>Saves the data.</summary>
        /// <param name="request">The request.</param>
        /// <exception cref="NotImplementedException"/>
        /// <returns></returns>
        public BaseResponse<TParam> SaveData(IDataChangeRequest<TParam, TData> request)
        {
            throw new NotImplementedException();
        }

        #endregion

        /// <summary>Clears the parameter sources.</summary>
        public void ClearParamSources()
        {
            var request = new ParameterSourceRequest();

            _paramSources.DeleteData(request);
        }

        /// <summary>Converts the query to request.</summary>
        /// <param name="request">The request.</param>
        /// <param name="operation">The operation.</param>
        /// <returns></returns>
        public DataQueryRequest<TParam, TData> ConvertQueryToRequest(IDataChangeRequest<TParam, TData> request,
                                                                     AdoOperation operation)
        {
            _guardian.CheckObject(GetQueryProcedure);

            //No paging, so just make normal request
            if(request.Page == null)
                return new DataQueryRequest<TParam, TData> {Param = request.Param, QueryProcedure = GetQueryProcedure(operation)};

            //For paging, save the parameter source
            _paramSources.SaveData(new ParameterSourceRequest {Data = request.Page});

            return new DataQueryRequest<TParam, TData> {Param = request.Param, QueryProcedure = GetQueryProcedure(operation)};
        }

        /// <summary>Makes the database request.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<DataSet> MakeDatabaseRequest(DataQueryRequest<TParam, TData> request)
        {
            _guardian.CheckObjectList(request, request.QueryProcedure);

            SbLog.It.Message(LogSeverity.Info,
                             request.Param != null ? "Parameter has been passed" : "No parameters passed");

            request.QueryProcedure.Param         = request.Param;
            request.QueryProcedure.ConnectionKey = request.ConnectionKey;
            var response = HandleQueryWithDatabase(request, request.QueryProcedure);

            return new BaseResponse<DataSet> {Data = response.Data, Success = true};
        }

        //public BaseResponse<TParam> SaveData(IDataChangeRequest<TParam, TData> request, IStoredProcedure<TParam, TData> getResponseData)
        //{
        //    //Use a re-try mechanism with Saves only
        //    Func<DataChangeRequest<TParam, TData>, BaseResponse<TParam>> op =
        //            r => new Rerunner().InvokeWithRetry<BaseResponse<TParam>, Exception>(() => MakeDatabaseChange(r));

        // var response = HandleChangeRequest(request, AdoOperation.Save, op);

        // ClearParamSources();

        //    return response;
        //}

        /// <summary>Converts the data change to request.</summary>
        /// <param name="request">The request.</param>
        /// <param name="operation">The operation.</param>
        /// <returns></returns>
        private DataChangeRequest<TParam, TData> ConvertDataChangeToRequest(IDataChangeRequest<TParam, TData> request, AdoOperation operation)
        {
            _guardian.CheckObject(GetChangeProcedure);

            return new DataChangeRequest<TParam, TData>
            {
                Param           = request.Param,
                Data            = request.Data,
                ChangeProcedure = GetChangeProcedure(operation)
            };
        }

        /// <summary>Executes the change.</summary>
        /// <param name="cmd">The command.</param>
        /// <returns></returns>
        private int ExecuteChange(SqlCommand cmd)
        {
            cmd.CommandTimeout = 300;
            _guardian.CheckObjectList(cmd, cmd.CommandText);

            //var adapter = new SqlDataAdapter { SelectCommand = cmd };

            var result = cmd.ExecuteScalar();

            var scopeId = result != null ? result.ToString().ToInt() : -1;
            cmd.Parameters.Clear();

            return scopeId;
        }

        /// <summary>Executes the query.</summary>
        /// <param name="cmd">The command.</param>
        /// <returns></returns>
        private DataSet ExecuteQuery(SqlCommand cmd)
        {
            cmd.CommandTimeout = 300;
            _guardian.CheckObjectList(cmd, cmd.CommandText);
            var adapter = new SqlDataAdapter {SelectCommand = cmd};

            SbLog.It.Message(LogSeverity.Info, $"The number of parameters is {cmd.Parameters.Count}");
            SbLog.It.Message(LogSeverity.Info, $"The command text is {cmd.CommandText}");

            var ds = new DataSet();
            adapter.Fill(ds);

            cmd.Parameters.Clear();

            return ds;
        }

        /// <summary>Gets the connection string.</summary>
        /// <param name="proc">The proc.</param>
        /// <returns></returns>
        private string GetConnectionString(IStoredProcedure<TParam, TData> proc)
        {
            if(!proc.ConnectionKey.IsNullOrEmpty())
                SbLog.It.Message(LogSeverity.Debug, $"The connection key is {proc.ConnectionKey}");

            var entry = ConfigurationManager.ConnectionStrings[proc.ConnectionKey];

            if(entry == null)
                throw new Exception($"Configuration {proc.ConnectionKey} does not exist");

            var connection = entry.ToString();
            SbLog.It.Message(LogSeverity.Trace, $"The connection string is {entry}");

            if(string.IsNullOrWhiteSpace(connection))
                throw new Exception("empty connection string");

            return connection;
        }

        /// <summary>Handles the change request.</summary>
        /// <param name="request">The request.</param>
        /// <param name="operation">The operation.</param>
        /// <param name="op">The op.</param>
        /// <returns></returns>
        private BaseResponse<TParam> HandleChangeRequest(IDataChangeRequest<TParam, TData> request,
                                                         AdoOperation operation,
                                                         Func<DataChangeRequest<TParam, TData>, BaseResponse<TParam>>
                                                                 op)
        {
            //_guardian.CheckObject(GetChangeProcedure);

            _guardian.CheckObject(request);

            var changeRequest = ConvertDataChangeToRequest(request, operation);

            var response = op(changeRequest);

            return new BaseResponse<TParam> {Data = request.Param, Success = response.Success};
        }

        /// <summary>Handles the change to database.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        private BaseResponse<int> HandleChangeToDatabase(DataChangeRequest<TParam, TData> request)
        {
            _guardian.CheckObjectList(request.ChangeProcedure);
            int scopeId;

            var cs = GetConnectionString(request.ChangeProcedure);

            using(var con = new SqlConnection(cs))
            {
                con.Open();

                TParam request1 = default;

                var cmd = request.ChangeProcedure.DefineCommand(con, request1,
                                                                _paramSources.GetList(request: null).Data);

                //SqlCommand DefineCommand(SqlConnection con, TParam inputParam, List<Parameter.IParameterSource> data);

                scopeId = ExecuteChange(cmd);
            }

            return new BaseResponse<int> {Data = scopeId, Success = true};
        }

        /// <summary>Handles the query with database.</summary>
        /// <param name="request">The request.</param>
        /// <param name="proc">The proc.</param>
        /// <returns></returns>
        private BaseResponse<DataSet> HandleQueryWithDatabase(IDataChangeRequest<TParam, TData> request,
                                                              IStoredProcedure<TParam, TData> proc)
        {
            //_guardian.CheckObjectList(request, request.QueryProcedure);

            SbLog.It.Message(LogSeverity.Info,
                             request.Param != null ? "Parameter has been passed" : "No parameters passed");

            //request.QueryProcedure.Key = request.Key;
            //request.QueryProcedure.ConnectionKey = request.ConnectionKey;

            _guardian.CheckObjectList(proc);

            //DataSet ds;

            var cs = GetConnectionString(proc);

            using(var con = new SqlConnection(cs))
            {
                con.Open();

                //var cmd = proc.DefineCommand(con, request, _paramSources.GetList(request: null).Data);

                //ds = ExecuteQuery(cmd);

                //ds.Validate();
            }

            //return new BaseResponse<DataSet> {Data = ds, Success = ds != null};
            return null;
        }

        /// <summary>Makes the database change.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        private BaseResponse<TParam> MakeDatabaseChange(DataChangeRequest<TParam, TData> request)
        {
            _guardian.CheckObjectList(request, request.ChangeProcedure);

            if(request.Param != null)
            {
                SbLog.It.Message(LogSeverity.Warn, "Object's natural parameter list being overridden");
                request.ChangeProcedure.Param = request.Param;
            }

            request.ChangeProcedure.ConnectionKey = request.ConnectionKey;
            var response = HandleChangeToDatabase(request);

            request.ChangeProcedure.ReadScopeIdentity(response.Data);

            return new BaseResponse<TParam> {Data = request.Param, Success = true};
        }

        #endregion
    }
}