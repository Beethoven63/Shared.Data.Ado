using Shared.Data.Ado.AdoConveyor.Change;
using Shared.Data.Ado.AdoConveyor.Query;
using Shared.FunctionalPattern;
using Shared.Logging.Responses;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Data.Ado.AdoConveyor.Conveyor
{
    /// <summary></summary>
    /// <typeparam name="TParam">The type of the parameter.</typeparam>
    /// <typeparam name="TData">The type of the data.</typeparam>
    public class BaseAdoConveyor<TParam, TData>: IDataConveyor<TParam, TData>
            where TData: class, new()
            where TParam: new()
    {
        #region Fields

        /// <summary>The guardian</summary>
        private readonly Guardian _guardian = new Guardian();

        #endregion

        #region Properties

        /// <summary>Gets the procedures.</summary>
        /// <value>The procedures.</value>
        protected Dictionary<AdoOperation, IStoredProcedure<TParam, TData>> Procedures{get;} =
            new Dictionary<AdoOperation, IStoredProcedure<TParam, TData>>();

        #endregion

        #region Methods

        /// <summary>Deletes the data.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<TParam> DeleteData(IDataChangeRequest<TParam, TData> request)
        {
            //Get the stored procedure
            var spResponse = GetStoredProcedure(request, AdoOperation.Delete);

            return spResponse.Success ? ChangeData(request, spResponse.Data) : new BaseResponse<TParam>();
        }

        /// <summary>Gets the item.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<TData> GetItem(IDataChangeRequest<TParam, TData> request)
        {
            _guardian.CheckObject(request);

            var qResponse = GetInitialList(request, AdoOperation.GetItem);

            //Check to see if any data in collection
            var list = qResponse.Data;

            if(!_guardian.CheckCollection(list, isEmptyValid: true, throwException: false))
                return new BaseResponse<TData>();

            //Make sure a single item is what is returned
            if(list.Count > 1)
                throw new Exception("Multiple records returned for single item query");

            //Convert from list to single item
            var item = list.FirstOrDefault();

            return item != null ? new BaseResponse<TData> {Data = item, Success = true} : new BaseResponse<TData>();
        }

        /// <summary>Gets the list.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<List<TData>> GetList(IDataChangeRequest<TParam, TData> request)
        {
            return GetInitialList(request, AdoOperation.GetList);
        }

        /// <summary>Saves the data.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<TParam> SaveData(IDataChangeRequest<TParam, TData> request)
        {
            //Get the stored procedure
            var spResponse = GetStoredProcedure(request, AdoOperation.Save);

            return spResponse.Success ? ChangeData(request, spResponse.Data) : new BaseResponse<TParam>();
        }

        /// <summary>Converts the <paramref name="data"/> to parameter.</summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        protected virtual TParam ConvertDataToParam(TData data)
        {
            return default;
        }

        /// <summary>Changes the data.</summary>
        /// <param name="request">The request.</param>
        /// <param name="sp">The sp.</param>
        /// <returns></returns>
        private BaseResponse<TParam> ChangeData(IDataChangeRequest<TParam, TData> request,
                                                IStoredProcedure<TParam, TData> sp)
        {
            var opResponse = sp.Operation == AdoOperation.Save ? InvokeWithRetry(sp) : sp.ExecuteChange();

            return !opResponse.Success ? new BaseResponse<TParam>() : new BaseResponse<TParam> {Data = request.Param, Success = opResponse.Success};
        }

        /// <summary>Gets the initial list.</summary>
        /// <param name="request">The request.</param>
        /// <param name="operation">The operation.</param>
        /// <returns></returns>
        private BaseResponse<List<TData>> GetInitialList(IDataChangeRequest<TParam, TData> request,
                                                         AdoOperation operation)
        {
            //Get the stored procedure
            var getResponse = GetStoredProcedure(request, operation);

            return getResponse.Success ? getResponse.Data.ExecuteQuery(request.Param) : new BaseResponse<List<TData>>();
        }

        /// <summary>Gets the stored procedure.</summary>
        /// <param name="request">The request.</param>
        /// <param name="operation">The operation.</param>
        /// <returns></returns>
        private BaseResponse<IStoredProcedure<TParam, TData>> GetStoredProcedure(IDataChangeRequest<TParam, TData> request, AdoOperation operation)
        {
            if(!Procedures.ContainsKey(operation))
                throw new Exception($"Unable to find stored procedure for key {operation}");

            var sp = Procedures[operation];

            AddParameter(request, operation, sp);

            return new BaseResponse<IStoredProcedure<TParam, TData>> {Data = sp, Success = true};
        }

        /// <summary>Adds the parameter.</summary>
        /// <param name="request">The request.</param>
        /// <param name="operation">The operation.</param>
        /// <param name="sp">The sp.</param>
        private void AddParameter(IDataChangeRequest<TParam, TData> request, AdoOperation operation, IStoredProcedure<TParam, TData> sp)
        {
            if(operation == AdoOperation.Save)
                sp.ParamObject = request.Param;

            if(request.Data != null)
                sp.ParamObject = ConvertDataToParam(request.Data);
        }

        /// <summary>Invokes the with retry.</summary>
        /// <param name="sp">The sp.</param>
        /// <returns></returns>
        private ChangeResponse InvokeWithRetry(IStoredProcedure<TParam, TData> sp)
        {
            return new Rerunner().InvokeWithRetry<ChangeResponse, Exception>(sp.ExecuteChange);
        }

        #endregion
    }
}