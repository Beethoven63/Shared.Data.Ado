using Shared.Data.Ado.AdoConveyor.Change;
using Shared.Logging.Responses;
using System.Collections.Generic;

namespace Shared.Data.Ado.AdoConveyor.Conveyor
{
    /// <summary></summary>
    /// <typeparam name="TParam">The type of the parameter.</typeparam>
    /// <typeparam name="TData">The type of the data.</typeparam>
    public interface IDataConveyor<TParam, TData>
    {
        #region Methods

        /// <summary>Deletes the data.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        BaseResponse<TParam> DeleteData(IDataChangeRequest<TParam, TData> request);

        /// <summary>Gets the item.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        BaseResponse<TData> GetItem(IDataChangeRequest<TParam, TData> request);

        /// <summary>Gets the list.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        BaseResponse<List<TData>> GetList(IDataChangeRequest<TParam, TData> request);

        /// <summary>Saves the data.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        BaseResponse<TParam> SaveData(IDataChangeRequest<TParam, TData> request);

        #endregion
    }
}