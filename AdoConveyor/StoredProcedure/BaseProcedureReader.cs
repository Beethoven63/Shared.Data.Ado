﻿using Shared.Data.Ado.AdoConveyor.Change;
using Shared.Data.Ado.DataConfiguration;
using Shared.Data.Ado.DbColumnAttribute;
using Shared.Data.Ado.Extensions;
using Shared.Data.Ado.Parameter;
using Shared.Data_Reader.Base;
using Shared.Exceptions;
using Shared.FunctionalPattern;
using Shared.Logging.MainLogging;
using Shared.Strings;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Shared.Data.Ado.AdoConveyor.StoredProcedure
{
    /// <summary></summary>
    /// <typeparam name="TRecord">The type of the record.</typeparam>
    /// <typeparam name="TParam">The type of the parameter.</typeparam>
    public class BaseProcedureReader<TRecord, TParam>: IBackgroundReader<TRecord, ProcedureRequest>
            where TRecord: class, IRecord, new()
    {
        #region Fields

        /// <summary>The connection key</summary>
        private string _connectionKey;

        /// <summary>The guardian</summary>
        private readonly Guardian _guardian = new Guardian();

        /// <summary>The unique identifier key</summary>
        private readonly Guid _guidKey = Guid.NewGuid();

        /// <summary>The helper</summary>
        private readonly DbColumnAttributeHelper _helper = new DbColumnAttributeHelper();

        /// <summary>The searcher</summary>
        private readonly DatabaseConfigSearcher _searcher = new DatabaseConfigSearcher();

        /// <summary>The worker</summary>
        private BackgroundWorker _worker;

        #endregion

        #region Properties

        /// <summary>Gets or sets the address.</summary>
        /// <value>The address.</value>
        public DataObjectAddress Address{get; set;}

        /// <summary>Gets or sets the connection key.</summary>
        /// <value>The connection key.</value>
        /// <exception cref="System.Exception">Unable to find connection key</exception>
        public string ConnectionKey
        {
            get
            {
                if(!_connectionKey.IsNullOrEmpty())
                    return _connectionKey;

                var response = _searcher.GetDataBase(Address);

                if(response.Success)
                    _connectionKey = response.Data.ConnectionKey;

                if(_connectionKey.IsNullOrEmpty())
                    throw new Exception("Unable to find connection key");

                return _connectionKey;
            }

            set => _connectionKey = value;
        }

        /// <summary>Gets or sets a value indicating whether this instance is read.</summary>
        /// <value><c>true</c> if this instance is read; otherwise, <c>false</c>.</value>
        public bool IsRead{get; set;}

        /// <summary>Gets or sets the on action.</summary>
        /// <value>The on action.</value>
        public Action OnReading{get; set;}

        /// <summary>Gets or sets the parameter object.</summary>
        /// <value>The parameter object.</value>
        public TParam ParamObject{get; set;}

        /// <summary>Gets or sets the record.</summary>
        /// <value>The record.</value>
        public List<TRecord> Records{get; set;}

        /// <summary>Gets or sets the response.</summary>
        /// <value>The response.</value>
        public ChangeResponse Response{get; set;}

        #endregion

        #region Methods

        #region IBackgroundReader<TRecord,ProcedureRequest> Members

        /// <summary>Initializes this instance.</summary>
        /// <param name="onReading">The on reading.</param>
        public void Initialize(Action onReading)
        {
            using(var action = new SingleAction(_guidKey.ToString()))
            {
                if(action.ActionTaken)
                    return;

                OnReading = onReading;

                _worker                    =  new BackgroundWorker();
                _worker.DoWork             += (s, e) => ThreadRead();
                _worker.RunWorkerCompleted += (s, e) => onReading?.Invoke();
            }
        }

        /// <summary>Reads the specified argument.</summary>
        /// <param name="request">The request.</param>
        public void Read(ProcedureRequest request)
        {
            if(_worker.IsBusy)
                return;

            _worker.RunWorkerAsync(request);
        }

        public void Start(ProcedureRequest argument)
        {
            //TODO_IMPLEMENT_ME();
        }

        public void Initialize(Action<RunWorkerCompletedEventArgs> onExecutionCompleted)
        {
            //TODO_IMPLEMENT_ME();
        }

        /// <summary>Resets this instance.</summary>
        public void Reset()
        {
            //TODO_IMPLEMENT_ME();
        }

        #endregion

        /// <summary>Defines the command.</summary>
        /// <param name="con">The con.</param>
        /// <param name="inputParam">The input parameter.</param>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public SqlCommand DefineCommand(SqlConnection con, TParam inputParam, List<IParameterSource> data)
        {
            throw new NotImplementedException();
        }

        /// <summary>Reads the scope identity.</summary>
        /// <param name="scopeIdentity">The scope identity.</param>
        public virtual void ReadScopeIdentity(int? scopeIdentity) {}

        /// <summary>Defines the command.</summary>
        /// <param name="con">The con.</param>
        /// <param name="paramObject">The parameter object.</param>
        /// <returns></returns>
        private SqlCommand DefineCommand(SqlConnection con, TParam paramObject)
        {
            return null;
        }

        /// <summary>Gets the connection string.</summary>
        /// <returns></returns>
        /// <exception cref="System.Exception">Configuration {ConnectionKey} does not exist or empty connection string</exception>
        /// <exception cref="Exception"><see cref="Configuration"/> {ConnectionKey} does not exist or empty connection string</exception>
        private string GetConnectionString()
        {
            if(!ConnectionKey.IsNullOrEmpty())
                SbLog.It.Message(LogSeverity.Debug, $"The connection key is {ConnectionKey}");

            var entry = ConfigurationManager.ConnectionStrings[ConnectionKey];

            if(entry == null)
                throw new Exception($"Configuration {ConnectionKey} does not exist");

            var connection = entry.ToString();
            SbLog.It.Message(LogSeverity.Trace, $"The connection string is {entry}");

            if(string.IsNullOrWhiteSpace(connection))
                throw new Exception("empty connection string");

            return connection;
        }

        /// <summary>Reads the result.</summary>
        /// <param name="result">The result.</param>
        private void ReadResult(object result)
        {
            if(result == null)
                return;

            //This suggests that if a response is returned, then success is true
            Response.ResponseReturned = true;

            ReadScopeIdentity(result.ToString().ToInt());

            //Todo:  The results are read but there is no data yet.

            Response.Success = true;
        }

        /// <summary>Threads the read.</summary>
        private void ThreadRead()
        {
            try
            {
                lock(this)
                {
                    var cmd        = new SqlCommand();
                    var inputParam = default(TParam);

                    try
                    {
                        Response = new ChangeResponse();

                        var cs = GetConnectionString();

                        using(var con = new SqlConnection(cs))
                        {
                            con.Open();

                            cmd = DefineCommand(con, inputParam);

                            var adapter = new SqlDataAdapter {SelectCommand = cmd};

                            SbLog.It.Message(LogSeverity.Info, $"The command text is {cmd.CommandText}");

                            var ds = new DataSet();
                            adapter.Fill(ds);

                            this.Records = ConvertDatasetToList(ds);

                            //success = true;
                        }
                    }
                    catch(Exception exception)
                    {
                        exception.Log();
                    }
                    finally
                    {
                        cmd?.Parameters.Clear();
                    }
                }
            }
            catch(Exception exception)
            {
                exception.Log();
            }
        }

        /// <summary>Rows to object.</summary>
        /// <param name="row">The row.</param>
        /// <returns></returns>
        public TRecord RowToObject(DataRow row)
        {
            var result = FromDataRow(row);

            _helper.AdjustThrougDbAttribute(result);

            return result;
        }

        /// <summary>Froms the data row.</summary>
        /// <param name="row">The row.</param>
        /// <returns></returns>
        public TRecord FromDataRow(DataRow row)
        {
            return null;
        }

        /// <summary>Converts the <paramref name="dataset"/> to list.</summary>
        /// <param name="dataset">The dataset.</param>
        /// <returns></returns>
        private List<TRecord> ConvertDatasetToList(DataSet dataset)
        {
            _guardian.CheckObject(dataset);
            _guardian.CheckObject(dataset.Tables);

            var table = dataset.Tables.ToList().FirstOrDefault();
            var list  = table.ToObjectList(RowToObject);

            return list;
        }

        #endregion
    }

    /// <summary></summary>
    public class ProcedureRequest {}
}