using Shared.Data.Ado.AdoConveyor.Change;
using Shared.Data.Ado.AdoConveyor.Query;
using Shared.Data.Ado.DataConfiguration;
using Shared.Data.Ado.DbColumnAttribute;
using Shared.Data.Ado.Extensions;
using Shared.Data.Ado.Parameter;
using Shared.Exceptions;
using Shared.FunctionalPattern;
using Shared.Logging.MainLogging;
using Shared.Logging.Responses;
using Shared.Strings;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Shared.Data.Ado.AdoConveyor.StoredProcedure
{
    /// <summary></summary>
    /// <typeparam name="TParam">The type of the parameter.</typeparam>
    /// <typeparam name="TData">The type of the data.</typeparam>
    public abstract class BaseStoredProcedure<TParam, TData>: IStoredProcedure<TParam, TData>
            where TData: class, new()
            where TParam: class
    {
        #region Fields

        /// <summary>The connection key</summary>
        private string _connectionKey;

        /// <summary>The converter</summary>
        private readonly DatabaseObjectStringConverter _converter = new DatabaseObjectStringConverter();

        /// <summary>The guardian</summary>
        private readonly Guardian _guardian = new Guardian();

        /// <summary>The helper</summary>
        private readonly DbColumnAttributeHelper _helper = new DbColumnAttributeHelper();

        /// <summary>The searcher</summary>
        private readonly DatabaseConfigSearcher _searcher = new DatabaseConfigSearcher();

        // ReSharper disable once MemberCanBePrivate.Global
        /// <summary>The guardian</summary>
        protected readonly Guardian Guardian = new Guardian();

        #endregion

        #region Properties

        /// <summary>Gets or sets the address.</summary>
        /// <value>The address.</value>
        public DataObjectAddress Address{get; set;}

        /// <summary>Gets or sets the connection key.</summary>
        /// <value>The connection key.</value>
        public string ConnectionKey
        {
            get
            {
                if(!_connectionKey.IsNullOrEmpty())
                    return _connectionKey;

                var response = _searcher.GetDataBase(Address);

                if(response.Success)
                    _connectionKey = response.Data.ConnectionKey;

                if(_connectionKey.IsNullOrEmpty())
                    throw new Exception("Unable to find connection key");

                return _connectionKey;
            }

            set => _connectionKey = value;
        }

        /// <summary>Gets or sets the name.</summary>
        /// <value>The name.</value>
        public string Name{get; set;}

        /// <summary>Gets or sets the operation.</summary>
        /// <value>The operation.</value>
        public AdoOperation Operation{get; set;}

        /// <summary>Gets or sets the parameter.</summary>
        /// <value>The parameter.</value>
        public object Param{get; set;}

        /// <summary>Gets or sets the parameters.</summary>
        /// <value>The parameters.</value>
        public List<SqlParameter> Parameters{get; set;}

        /// <summary>Gets or sets the parameter object.</summary>
        /// <value>The parameter object.</value>
        public TParam ParamObject{get; set;}

        /// <summary>Gets or sets the number of parameters expected.</summary>
        /// <value>The number of parameters expected.</value>
        protected int NumberOfParametersExpected{get; set;}

        /// <summary>Gets the parameter provider.</summary>
        /// <value>The parameter provider.</value>
        protected BaseParameterProvider ParameterProvider{get;} = new BaseParameterProvider();

        #endregion

        #region Methods

        #region IStoredProcedure<TParam,TData> Members

        /// <summary>Defines the command.</summary>
        /// <param name="con">The con.</param>
        /// <param name="inputParam">The input parameter.</param>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        public SqlCommand DefineCommand(SqlConnection con, TParam inputParam, List<IParameterSource> data)
        {
            throw new NotImplementedException();
        }

        /// <summary>Executes the change.</summary>
        /// <returns></returns>
        public ChangeResponse ExecuteChange()
        {
            var        success          = false;
            var        responseReturned = false;
            SqlCommand cmd              = null;

            try
            {
                var cs = GetConnectionString();

                using(var con = new SqlConnection(cs))
                {
                    con.Open();

                    cmd = DefineCommand(con, ParamObject);

                    var result = cmd.ExecuteScalar();
                    responseReturned = result != null;

                    if(responseReturned)
                        ReadScopeIdentity(result.ToString().ToInt());

                    success = true;
                }
            }
            catch(Exception exception)
            {
                exception.Log();
            }
            finally
            {
                cmd?.Parameters.Clear();
            }

            return new ChangeResponse {ResponseReturned = responseReturned, Success = success};
        }

        /// <summary>Executes the query.</summary>
        /// <param name="inputParam">The input parameter.</param>
        /// <returns></returns>
        public BaseResponse<List<TData>> ExecuteQuery(TParam inputParam)
        {
            var success = false;

            var list = new List<TData>();

            try
            {
                var cs = GetConnectionString();

                using(var con = new SqlConnection(cs))
                {
                    con.Open();

                    var cmd = DefineCommand(con, inputParam);

                    var adapter = new SqlDataAdapter {SelectCommand = cmd};

                    SbLog.It.Message(LogSeverity.Info, $"The command text is {cmd.CommandText}");

                    var ds = new DataSet();
                    adapter.Fill(ds);

                    list = ConvertDatasetToList(ds);

                    success = true;
                }
            }
            catch(Exception exception)
            {
                exception.Log();
            }

            return new BaseResponse<List<TData>> {Data = list, Success = success};
        }

        /// <summary>Froms the data row.</summary>
        /// <param name="row">The row.</param>
        /// <returns></returns>
        public abstract TData FromDataRow(DataRow row);

        /// <summary>Determines whether [is parameter valid].</summary>
        /// <returns><c>true</c> if [is parameter valid]; otherwise, <c>false</c> .</returns>
        public virtual bool IsParamValid()
        {
            return true;
        }

        /// <summary>Reads the scope identity.</summary>
        /// <param name="scopeIdentity">The scope identity.</param>
        public virtual void ReadScopeIdentity(int scopeIdentity) {}

        /// <summary>Rows to object.</summary>
        /// <param name="row">The row.</param>
        /// <returns></returns>
        public TData RowToObject(DataRow row)
        {
            var result = FromDataRow(row);

            _helper.AdjustThrougDbAttribute(result);

            return result;
        }

        #endregion

        /// <summary>Creates the parameter list.</summary>
        /// <returns></returns>
        public abstract List<SqlParameter> CreateParameterList();

        /// <summary>Defines the command.</summary>
        /// <param name="con">The con.</param>
        /// <param name="inputParam">The input parameter.</param>
        /// <returns></returns>
        public SqlCommand DefineCommand(SqlConnection con, TParam inputParam)
        {
            var cmd = new SqlCommand
            {
                CommandType    = CommandType.StoredProcedure,
                CommandText    = Address.DbObject,
                Connection     = con,
                CommandTimeout = 300
            };

            BuildParameters(cmd, inputParam);

            return cmd;
        }

        /// <summary>Stores the address.</summary>
        /// <param name="address">The address.</param>
        protected void StoreAddress(string address)
        {
            Address = (DataObjectAddress)_converter.ConvertFrom(address);
        }

        // ReSharper disable once UnusedParameter.Local
        /// <summary>Adds the parameters.</summary>
        /// <param name="paramSources">The parameter sources.</param>
        private void AddParams(List<IParameterSource> paramSources)
        {
            //SbLog.It.Message(LogSeverity.Info, "The number of parameters expected by the stored procedure is {0}", NumberOfParametersExpected);

            //if (ParamObject == null)
            //    return;

            //if (NumberOfParametersExpected != 0)
            //    throw new Exception(string.Format("The number of parameters expected by the stored procedure is {0}, but the param object is null", this.NumberOfParametersExpected));

            //if (!IsParamValid())
            //    throw new Exception("The parameter data object is invalid");

            //Parameters = CreateParameterList();
            //SbLog.It.Message(LogSeverity.Info, "{0} parameter(s) created", Parameters.Count);

            //if (Parameters.Count != NumberOfParametersExpected)
            //    throw new Exception(string.Format("The number of parameters expected by the stored procedure is {0}, and the actual number of parameters created is (1)", this.NumberOfParametersExpected, Parameters.Count));

            ////If parameters added, add to  paramsource
            //if (Parameters.Any())
            //    paramSources.Add(this);
        }

        /// <summary>Builds the parameters.</summary>
        /// <param name="cmd">The command.</param>
        /// <param name="paramObject">The parameter object.</param>
        private void BuildParameters(SqlCommand cmd, TParam paramObject)
        {
            if(paramObject == null)
                return;

            //The param object for the sp comes from the request or from the object being saved
            //ParamObject is used inside CreateParameterList to make the parameters

            ParamObject = paramObject;

            var list = CreateParameterList();

            cmd.AddParameters(list);
        }

        /// <summary>Converts the <paramref name="dataset"/> to list.</summary>
        /// <param name="dataset">The dataset.</param>
        /// <returns></returns>
        private List<TData> ConvertDatasetToList(DataSet dataset)
        {
            _guardian.CheckObject(dataset);
            _guardian.CheckObject(dataset.Tables);

            var table = dataset.Tables.ToList().FirstOrDefault();
            var list  = table.ToObjectList(RowToObject);

            return list;
        }

        /// <summary>Gets the connection string.</summary>
        /// <returns></returns>
        private string GetConnectionString()
        {
            if(!ConnectionKey.IsNullOrEmpty())
                SbLog.It.Message(LogSeverity.Debug, $"The connection key is {ConnectionKey}");

            var entry = ConfigurationManager.ConnectionStrings[ConnectionKey];

            if(entry == null)
                throw new Exception($"Configuration {ConnectionKey} does not exist");

            var connection = entry.ToString();
            SbLog.It.Message(LogSeverity.Trace, $"The connection string is {entry}");

            if(string.IsNullOrWhiteSpace(connection))
                throw new Exception("empty connection string");

            return connection;
        }

        #endregion
    }
}