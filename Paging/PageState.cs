namespace Shared.Data.Ado.Paging
{
    /// <summary></summary>
    public enum PageState
    {
        /// <summary>The initial state</summary>
        InitialState,

        /// <summary>The parent header written</summary>
        ParentHeaderWritten
    }
}