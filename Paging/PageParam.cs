using Shared.Strings;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace Shared.Data.Ado.Paging
{
    /// <summary></summary>
    public class PageParam: IPageParam
    {
        //private readonly BaseParameterProvider _parameterProvider = new BaseParameterProvider();

        #region Fields

        /// <summary>The page size</summary>
        private int _pageSize;

        #endregion

        #region Properties

        /// <summary>Gets or sets the end date.</summary>
        /// <value>The end date.</value>
        public DateTime EndDate{get; set;}

        /// <summary>Gets or sets the number of parameters expected.</summary>
        /// <value>The number of parameters expected.</value>
        public int NumberOfParametersExpected{get; set;}

        /// <summary>Gets the page key.</summary>
        /// <value>The page key.</value>
        public string PageKey => PageNumber.ToString();

        /// <summary>Gets or sets the page number.</summary>
        /// <value>The page number.</value>
        public int PageNumber{get; set;} = 1;

        /// <summary>Gets the size of the page.</summary>
        /// <value>The size of the page.</value>
        public int PageSize
        {
            get
            {
                if(_pageSize == 0)
                    _pageSize = ConfigurationManager.AppSettings["PageSize"].ToInt();

                return _pageSize;
            }
        }

        /// <summary>Gets or sets the parameters.</summary>
        /// <value>The parameters.</value>
        public List<SqlParameter> Parameters
        {
            //if (!_parameters.Any())
            //CreateParameterList();
            get;
            set;
        } = new List<SqlParameter>();

        /// <summary>Gets or sets the source key.</summary>
        /// <value>The source key.</value>
        public string SourceKey{get; set;}

        /// <summary>Gets or sets the start date.</summary>
        /// <value>The start date.</value>
        public DateTime StartDate{get; set;}

        /// <summary>Gets or sets the state.</summary>
        /// <value>The state.</value>
        public PageState State{get; set;}

        #endregion

        #region Methods

        /// <summary>Checks the state of the page.</summary>
        /// <returns></returns>
        public bool CheckPageState()
        {
            return State == PageState.InitialState;
        }

        //public List<SqlParameter> CreateParameterList()
        //{
        //    if (_parameters.Any())
        //        return _parameters;

        // _parameters.Add(_parameterProvider.CreateSqlParameter("@START_DATE", SqlDbType.DateTime,
        // StartDate)); _parameters.Add(_parameterProvider.CreateSqlParameter("@END_DATE",
        // SqlDbType.DateTime, EndDate));
        // _parameters.Add(_parameterProvider.CreateSqlParameter("@PAGE_NUMBER", SqlDbType.Int,
        // PageNumber)); _parameters.Add(_parameterProvider.CreateSqlParameter("@PAGESIZE",
        // SqlDbType.Int, PageSize));

        //    return _parameters;
        //}

        /// <summary>Updates this instance.</summary>
        public void Update()
        {
            PageNumber++;

            UpdateState();
        }

        /// <summary>Updates the state.</summary>
        public void UpdateState()
        {
            if(State == PageState.ParentHeaderWritten)
                State = PageState.InitialState;
            else
                State++;
        }

        #endregion
    }
}