using Shared.Strings;
using System.Configuration;

namespace Shared.Data.Ado.Paging
{
    /// <summary></summary>
    public class PageData
    {
        #region Fields

        /// <summary>The page size</summary>
        private int _pageSize;

        #endregion

        #region Properties

        /// <summary>Gets or sets the page number.</summary>
        /// <value>The page number.</value>
        public int PageNumber{get; set;} = 1;

        /// <summary>Gets the size of the page.</summary>
        /// <value>The size of the page.</value>
        public int PageSize
        {
            get
            {
                if(_pageSize == 0)
                    _pageSize = ConfigurationManager.AppSettings["PageSize"].ToInt();

                return _pageSize;
            }
        }

        #endregion

        #region Methods

        /// <summary>Updates this instance.</summary>
        public void Update()
        {
            PageNumber++;

            UpdateState();
        }

        /// <summary>Updates the state.</summary>
        public void UpdateState()
        {
            //if (State == PageState.ChildHeaderWritten)
            //    State = PageState.InitialState;
            //else
            //    this.State++;
        }

        #endregion
    }
}