using Shared.Data.Ado.Parameter;
using System;

namespace Shared.Data.Ado.Paging
{
    /// <summary></summary>
    public interface IPageParam: IParameterSource
    {
        #region Properties

        /// <summary>Gets or sets the end date.</summary>
        /// <value>The end date.</value>
        DateTime EndDate{get; set;}

        /// <summary>Gets or sets the page number.</summary>
        /// <value>The page number.</value>
        int PageNumber{get; set;}

        /// <summary>Gets the size of the page.</summary>
        /// <value>The size of the page.</value>
        int PageSize{get;}

        /// <summary>Gets or sets the start date.</summary>
        /// <value>The start date.</value>
        DateTime StartDate{get; set;}

        /// <summary>Gets or sets the state.</summary>
        /// <value>The state.</value>
        PageState State{get; set;}

        #endregion

        #region Methods

        /// <summary>Checks the state of the page.</summary>
        /// <returns></returns>
        bool CheckPageState();

        /// <summary>Updates the state.</summary>
        void UpdateState();

        #endregion
    }
}