using Shared.Data.Ado.AdoConveyor.Change;
using System;

namespace Shared.Data.Ado.Paging
{
    /// <summary></summary>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    public class PropertyRequest<TKey>: IDataChangeRequest<TKey, int> //TODO_JOE:  THIS LATTER PARAMETER IS A HACK
    {
        #region Properties

        /// <summary>Gets or sets the data.</summary>
        /// <value>The data.</value>
        public int Data{get; set;}

        /// <summary>Gets or sets the name.</summary>
        /// <value>The name.</value>
        public string Name{get; set;}

        /// <summary>Gets or sets the page.</summary>
        /// <value>The page.</value>
        public PageParam Page{get; set;}

        /// <summary>Gets or sets the parameter.</summary>
        /// <value>The parameter.</value>
        public TKey Param{get; set;}

        /// <summary>Gets or sets the type.</summary>
        /// <value>The type.</value>
        public Type Type{get; set;}

        #endregion
    }
}