namespace Shared.Data.Ado.Paging
{
    /// <summary></summary>
    internal class LoopResponse
    {
        #region Properties

        /// <summary>Gets or sets a value indicating whether this <see cref="LoopResponse"/> is break.</summary>
        /// <value><c>true</c> if break; otherwise, <c>false</c> .</value>
        public bool Break{get; set;}

        /// <summary>Gets or sets a value indicating whether this <see cref="LoopResponse"/> is success.</summary>
        /// <value><c>true</c> if success; otherwise, <c>false</c> .</value>
        public bool Success{get; set;}

        #endregion
    }
}