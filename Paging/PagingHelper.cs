using Shared.Logging.Responses;
using System;

namespace Shared.Data.Ado.Paging
{
    /// <summary></summary>
    public class PagingHelper
    {
        #region Methods

        /// <summary>Paginizes the action.</summary>
        /// <typeparam name="TRequest">The type of the request.</typeparam>
        /// <param name="request">The request.</param>
        /// <param name="action">The action.</param>
        /// <returns></returns>
        private BaseResponse<int> PaginizeAction<TRequest>(TRequest request, Func<TRequest, LoopResponse> action)
        {
            var  page = new PageData();
            bool success;

            while(true)
            {
                //request.DefineParam(page);

                var response = action(request);
                success = response.Success;

                if(response.Break)
                    break;

                page.Update();

                //EndOfPage(request);
            }

            return new BaseResponse<int>
            {
                Success = success
            };
        }

        #endregion
    }
}