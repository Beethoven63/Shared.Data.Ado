namespace Shared.Data.Ado.DbColumnAttribute
{
    /// <summary></summary>
    /// <typeparam name="TData">The type of the data.</typeparam>
    public interface IAdjuster<TData>
    {
        #region Methods

        /// <summary>Adjusts the specified input.</summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        TData Adjust(TData input);

        #endregion
    }
}