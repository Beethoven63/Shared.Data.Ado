using System;
using System.Data;

namespace Shared.Data.Ado.DbColumnAttribute
{
    /// <summary></summary>
    public class DbColumnAttribute: Attribute
    {
        #region Properties

        /// <summary>Gets or sets the alias.</summary>
        /// <value>The alias.</value>
        public string Alias{get; set;}

        /// <summary>Gets or sets the type of the converter.</summary>
        /// <value>The type of the converter.</value>
        public Type ConverterType{get; set;}

        /// <summary>Gets or sets the display order.</summary>
        /// <value>The display order.</value>
        public int DisplayOrder{get; set;}

        /// <summary>Gets or sets the name.</summary>
        /// <value>The name.</value>
        public string Name{get; set;}

        /// <summary>Gets or sets a value indicating whether this <see cref="DbColumnAttribute"/> is required.</summary>
        /// <value><c>true</c> if required; otherwise, <c>false</c> .</value>
        public bool Required{get; set;}

        /// <summary>Gets or sets the type.</summary>
        /// <value>The type.</value>
        public SqlDbType Type{get; set;}

        #endregion

        #region Constructors

        /// <summary>Initializes a new instance of the <see cref="DbColumnAttribute"/> class.</summary>
        /// <param name="name">The name.</param>
        /// <param name="type">The type.</param>
        /// <param name="required">if set to <c>true</c> [required].</param>
        /// <param name="converterType">Type of the converter.</param>
        public DbColumnAttribute(string name, SqlDbType type, bool required = true, Type converterType = null)
        {
            Name          = name;
            Type          = type;
            ConverterType = converterType;
            Required      = required;
        }

        #endregion
    }
}