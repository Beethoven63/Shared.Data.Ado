using System;
using System.Reflection;

namespace Shared.Data.Ado.DbColumnAttribute
{
    /// <summary></summary>
    public class PropertyInfoData
    {
        #region Properties

        /// <summary>Gets or sets the alias.</summary>
        /// <value>The alias.</value>
        public string Alias{get; set;}

        /// <summary>Gets the name.</summary>
        /// <value>The name.</value>
        public string Name{get; internal set;}

        /// <summary>Gets or sets the order.</summary>
        /// <value>The order.</value>
        public int Order{get; set;}

        /// <summary>Gets or sets the property.</summary>
        /// <value>The property.</value>
        public PropertyInfo Property{get; set;}

        #endregion

        #region Methods

        /// <summary>Clones this instance.</summary>
        /// <exception cref="NotImplementedException"/>
        /// <returns></returns>
        public object Clone()
        {
            throw new NotImplementedException();
        }

        /// <summary>Compares to.</summary>
        /// <param name="other">The other.</param>
        /// <exception cref="NotImplementedException"/>
        /// <returns></returns>
        public int CompareTo(PropertyInfoData other)
        {
            throw new NotImplementedException();
        }

        /// <summary>Equalses the specified other.</summary>
        /// <param name="other">The other.</param>
        /// <exception cref="NotImplementedException"/>
        /// <returns></returns>
        public bool Equals(PropertyInfoData other)
        {
            throw new NotImplementedException();
        }

        /// <summary>Converts to string.</summary>
        /// <returns>A <see cref="String"/> that represents this instance.</returns>
        public override string ToString()
        {
            return Property.Name;
        }

        #endregion
    }
}