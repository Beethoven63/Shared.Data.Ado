using System.Collections.Generic;
using System.Linq;

namespace Shared.Data.Ado.DbColumnAttribute
{
    /// <summary></summary>
    public class NameAdjuster: IAdjuster<string>
    {
        #region Fields

        /// <summary>The illegal characters</summary>
        private readonly List<char> _illegalCharacters;

        #endregion

        #region Constructors

        /// <summary>Initializes a new instance of the <see cref="NameAdjuster"/> class.</summary>
        public NameAdjuster()
        {
            _illegalCharacters = "-".ToCharArray().ToList();
        }

        #endregion

        #region Methods

        /// <summary>Adjusts the specified input.</summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public string Adjust(string input)
        {
            if(!_illegalCharacters.Any(input.Contains))
                return input;

            var charsFound = _illegalCharacters.Where(input.Contains);

            return charsFound.Aggregate(input, (current, ch) => current.Replace(ch, newChar: '_'));
        }

        #endregion
    }
}