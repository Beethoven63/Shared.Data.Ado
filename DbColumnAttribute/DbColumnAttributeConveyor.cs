using Shared.Data.Ado.AdoConveyor.Change;
using Shared.Data.Ado.AdoConveyor.Conveyor;
using Shared.Extension;
using Shared.FunctionalPattern;
using Shared.Logging.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Shared.Data.Ado.DbColumnAttribute
{
    /// <summary></summary>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    public class DbColumnAttributeConveyor<TKey>: IDataConveyor<TKey, PropertyInfoData>
    {
        #region Fields

        /// <summary>The guardian</summary>
        private readonly Guardian _guardian = new Guardian();

        #endregion

        #region Methods

        #region IDataConveyor<TKey,PropertyInfoData> Members

        /// <summary>Deletes the data.</summary>
        /// <param name="request">The request.</param>
        /// <exception cref="NotImplementedException"/>
        /// <returns></returns>
        public BaseResponse<TKey> DeleteData(IDataChangeRequest<TKey, PropertyInfoData> request)
        {
            throw new NotImplementedException();
        }

        /// <summary>Gets the item.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<PropertyInfoData> GetItem(IDataChangeRequest<TKey, PropertyInfoData> request)
        {
            // Get the properties of the data class that have the correct attribute
            var properties = GetProperties(request.Type);

            //Find the property with the matching name
            var matchingProperty = properties.FirstOrDefault(p => p.Name == request.Name);
            _guardian.CheckObject(matchingProperty);

            return new BaseResponse<PropertyInfoData> {Data = new PropertyInfoData {Property = matchingProperty}, Success = true};
        }

        /// <summary>Gets the list.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<List<PropertyInfoData>> GetList(IDataChangeRequest<TKey, PropertyInfoData> request)
        {
            // Get the properties of the data class that have the correct attribute
            var properties = GetProperties(request.Type);

            return new BaseResponse<List<PropertyInfoData>> {Data = properties.Select(p => new PropertyInfoData {Property = p}).ToList(), Success = true};
        }

        /// <summary>Saves the data.</summary>
        /// <param name="request">The request.</param>
        /// <exception cref="NotImplementedException"/>
        /// <returns></returns>
        public BaseResponse<TKey> SaveData(IDataChangeRequest<TKey, PropertyInfoData> request)
        {
            throw new NotImplementedException();
        }

        #endregion

        /// <summary>Gets the properties.</summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        private List<PropertyInfo> GetProperties(Type type)
        {
            var properties = type.GetProperties().Where(HasDbColumnAttribute).ToList();

            _guardian.CheckCollection(properties, isEmptyValid: false);

            return properties;
        }

        /// <summary>Determines whether [has database column attribute] [the specified property].</summary>
        /// <param name="property">The property.</param>
        /// <returns><c>true</c> if [has database column attribute] [the specified property]; otherwise, <c>false</c> .</returns>
        private bool HasDbColumnAttribute(PropertyInfo property)
        {
            var attrib = property.GetFilteredAttribute<DbColumnAttribute>();

            return attrib != null;
        }

        #endregion
    }
}