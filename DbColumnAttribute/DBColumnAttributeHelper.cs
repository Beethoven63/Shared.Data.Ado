using Shared.Collection;
using Shared.Extension;
using Shared.FunctionalPattern;
using Shared.Logging.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Shared.Data.Ado.DbColumnAttribute
{
    /// <summary></summary>
    public class DbColumnAttributeHelper
    {
        #region Fields

        /// <summary>The guardian</summary>
        private readonly Guardian _guardian = new Guardian();

        #endregion

        #region Methods

        /// <summary>Adjusts the through database attribute.</summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        public TData AdjustThrougDbAttribute<TData>(TData result)
        {
            var response = GetAdjusters<TData>();

            if(!response.Success)
                return result;

            foreach(var adj in response.Data)
                ChangeProperty<TData, string>(result, adj.Property, v => ChangePropertyValue(v, adj));

            return result;
        }

        /// <summary>Gets the adjusters.</summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <returns></returns>
        public BaseResponse<List<AdjusterResponse<TData>>> GetAdjusters<TData>()
        {
            var type = typeof(TData);

            var responses = type.GetProperties().Select(GetAdjustFunction<TData>).Where(r => r.Success).ToList();

            return new BaseResponse<List<AdjusterResponse<TData>>>
            {
                Data    = responses,
                Success = !responses.IsNullOrEmpty()
            };
        }

        /// <summary>Gets the alias.</summary>
        /// <param name="property">The property.</param>
        /// <returns></returns>
        public string GetAlias(PropertyInfo property)
        {
            var attrib = property.GetFilteredAttribute<DbColumnAttribute>();

            return attrib != null ? attrib.Alias : string.Empty;
        }

        /// <summary>Gets the database column properties.</summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public BaseResponse<List<PropertyInfo>> GetDbColumnProperties(Type type)
        {
            _guardian.CheckObjectList(type);

            // Get the properties of the data class that have the correct attribute
            var properties = type.GetProperties().Where(HasDbColumnAttribute).ToList();

            return new BaseResponse<List<PropertyInfo>>
            {
                Data    = properties,
                Success = true
            };
        }

        /// <summary>Gets the matching property.</summary>
        /// <param name="type">The type.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public BaseResponse<PropertyInfo> GetMatchingProperty(Type type, string name)
        {
            var response = GetDbColumnProperties(type);

            if(!response.Success)
                return new BaseResponse<PropertyInfo>();

            var prop = response.Data.Where(IsRequired).FirstOrDefault(p => HasSameName(p, name));

            return new BaseResponse<PropertyInfo>
            {
                Data    = prop,
                Success = prop != null
            };
        }

        /// <summary>Gets the order.</summary>
        /// <param name="property">The property.</param>
        /// <returns></returns>
        public int GetOrder(PropertyInfo property)
        {
            var attrib = property.GetFilteredAttribute<DbColumnAttribute>();

            return attrib?.DisplayOrder ?? 0;
        }

        /// <summary>Determines whether the specified <paramref name="property"/> is required.</summary>
        /// <param name="property">The property.</param>
        /// <returns><c>true</c> if the specified <paramref name="property"/> is required; otherwise, <c>false</c> .</returns>
        public bool IsRequired(PropertyInfo property)
        {
            var attrib = property.GetFilteredAttribute<DbColumnAttribute>(a => a.Required);

            return attrib != null;
        }

        /// <summary>Changes the property.</summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="field">The field.</param>
        /// <param name="prop">The property.</param>
        /// <param name="changeValue">The change value.</param>
        private void ChangeProperty<TData, TValue>(TData field, PropertyInfo prop, Func<TValue, TValue> changeValue)
        {
            //Get the value
            var value = (TValue)prop.GetValue(field);

            //Change the value
            value = changeValue(value);

            //change the property
            prop.SetValue(field, value);
        }

        /// <summary>Changes the property value.</summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="value">The value.</param>
        /// <param name="adj">The adj.</param>
        /// <returns></returns>
        private string ChangePropertyValue<TData>(string value, AdjusterResponse<TData> adj)
        {
            var adjuster = (IAdjuster<string>)Activator.CreateInstance(adj.ConverterType);

            return adjuster.Adjust(value);
        }

        /// <summary>Gets the adjust function.</summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="property">The property.</param>
        /// <returns></returns>
        private AdjusterResponse<TData> GetAdjustFunction<TData>(PropertyInfo property)
        {
            var attrib = property.GetFilteredAttribute<DbColumnAttribute>(a => a.ConverterType != null);

            if(attrib == null)
                return new AdjusterResponse<TData>();

            return new AdjusterResponse<TData>
            {
                Success       = true,
                Property      = property,
                ConverterType = attrib.ConverterType
            };
        }

        /// <summary>Determines whether [has database column attribute] [the specified property].</summary>
        /// <param name="property">The property.</param>
        /// <returns><c>true</c> if [has database column attribute] [the specified property]; otherwise, <c>false</c> .</returns>
        private bool HasDbColumnAttribute(PropertyInfo property)
        {
            var attrib = property.GetFilteredAttribute<DbColumnAttribute>();

            return attrib != null;
        }

        /// <summary>Determines whether [has same name] [the specified property].</summary>
        /// <param name="property">The property.</param>
        /// <param name="name">The name.</param>
        /// <returns><c>true</c> if [has same name] [the specified property]; otherwise, <c>false</c> .</returns>
        private bool HasSameName(PropertyInfo property, string name)
        {
            var attrib = property.GetFilteredAttribute<DbColumnAttribute>(a => a.Name == name);

            return attrib != null;
        }

        #endregion
    }
}