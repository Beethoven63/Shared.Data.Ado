using Shared.Logging.Responses;
using System;
using System.Reflection;

namespace Shared.Data.Ado.DbColumnAttribute
{
    /// <summary></summary>
    /// <typeparam name="TData">The type of the data.</typeparam>
    public class AdjusterResponse<TData>: BaseResponse<TData>
    {
        #region Properties

        /// <summary>Gets the type of the converter.</summary>
        /// <value>The type of the converter.</value>
        public Type ConverterType{get; internal set;}

        /// <summary>Gets the property.</summary>
        /// <value>The property.</value>
        public PropertyInfo Property{get; internal set;}

        #endregion
    }
}