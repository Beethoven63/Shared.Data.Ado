using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Shared.Data.Ado.Extensions
{
    /// <summary></summary>
    public static class SqlParameterCollectionExtension
    {
        #region Methods

        /// <summary>Converts to list.</summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static List<SqlParameter> ToList(this SqlParameterCollection parameters)
        {
            return parameters.OfType<SqlParameter>().ToList();
        }

        #endregion
    }
}