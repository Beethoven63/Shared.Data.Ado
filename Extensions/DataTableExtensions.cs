using Shared.FunctionalPattern;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Shared.Data.Ado.Extensions
{
    /// <summary></summary>
    public static class DataTableExtensions
    {
        #region Methods

        /// <summary>Adds the row.</summary>
        /// <param name="table">The table.</param>
        /// <param name="rowString">The row string.</param>
        /// <param name="parser">The parser.</param>
        public static void AddRow(this DataTable table, string rowString, Func<string, List<string>> parser)
        {
            var row = table.NewRow();
            new Guardian().CheckObject(rowString);

            //var itemCount = row.ItemArray.Count();

            var values = parser(rowString).Cast<object>();

            row.ItemArray = values.ToArray();
            table.Rows.Add(row);
        }

        /// <summary>Adds the string column.</summary>
        /// <param name="table">The table.</param>
        /// <param name="title">The title.</param>
        public static void AddStringColumn(this DataTable table, string title)
        {
            table.Columns.Add(title, typeof(string));
        }

        /// <summary>Determines whether [is <see langword="null"/> or empty].</summary>
        /// <param name="table">The table.</param>
        /// <returns><c>true</c> if [is <see langword="null"/> or empty] [the specified table]; otherwise, <c>false</c> .</returns>
        public static bool IsNullOrEmpty(this DataTable table)
        {
            return table == null || table.Rows.Count == 0;
        }

        /// <summary>Converts to object list.</summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="table">The table.</param>
        /// <param name="converter">The converter.</param>
        /// <returns></returns>
        public static List<TData> ToObjectList<TData>(this DataTable table, Func<DataRow, TData> converter)
                where TData: class
        {
            var g = new Guardian();
            g.CheckObjectList(table, converter);
            var rowList = table.ToRowList();

            if(!g.CheckCollection(rowList, isEmptyValid: true, throwException: false))
                return new List<TData>();

            var itemList = rowList.Select(converter).ToList();

            return g.CheckCollection(itemList, isEmptyValid: true, throwException: false)
                           ? itemList
                           : new List<TData>();
        }

        /// <summary>Converts to rowlist.</summary>
        /// <param name="table">The table.</param>
        /// <returns></returns>
        public static List<DataRow> ToRowList(this DataTable table)
        {
            return table.Rows.OfType<DataRow>().ToList();
        }

        #endregion
    }
}