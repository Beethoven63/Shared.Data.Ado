using Shared.FunctionalPattern;
using System.Data;

namespace Shared.Data.Ado.Extensions
{
    /// <summary></summary>
    public static class DataSetExtensions
    {
        #region Fields

        /// <summary>The guardian</summary>
        private static readonly Guardian Guardian = new Guardian();

        #endregion

        #region Methods

        /// <summary>Validates the specified ds.</summary>
        /// <param name="ds">The ds.</param>
        public static void Validate(this DataSet ds)
        {
            Guardian.CheckObject(ds);
            Guardian.CheckObject(ds.Tables);

            foreach(DataTable table in ds.Tables)
            {
                foreach(DataRow row in table.Rows)
                    row.Validate();
            }
        }

        #endregion
    }
}