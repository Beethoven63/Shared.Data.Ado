using Shared.FunctionalPattern;
using Shared.Strings;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Shared.Data.Ado.Extensions
{
    /// <summary></summary>
    public static class DataRowExtensions
    {
        #region Fields

        /// <summary>The guardian</summary>
        private static readonly Guardian Guardian = new Guardian();

        #endregion

        #region Methods

        /// <summary>Converts to stringlist.</summary>
        /// <param name="row">The row.</param>
        /// <returns></returns>
        public static List<string> ToStringList(this DataRow row)
        {
            return row.ItemArray.ToList()
                      .Select(s => s.ToString().Surround("'"))
                      .ToList();
        }

        /// <summary>Trims the specified field name.</summary>
        /// <param name="row">The row.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        public static string Trim(this DataRow row, string fieldName)
        {
            return row[fieldName].ToString().Trim();
        }

        /// <summary>Validates the specified row.</summary>
        /// <param name="row">The row.</param>
        public static void Validate(this DataRow row)
        {
            Guardian.CheckObject(row);
            Guardian.CheckObject(row.ItemArray);

            var number = row.ItemArray.FirstOrDefault();
            Guardian.CheckObject(number);
        }

        #endregion
    }
}