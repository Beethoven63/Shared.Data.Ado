using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Shared.Data.Ado.Extensions
{
    /// <summary></summary>
    public static class DataTableCollectionExtension
    {
        #region Methods

        /// <summary>Converts to list.</summary>
        /// <param name="tables">The tables.</param>
        /// <returns></returns>
        public static List<DataTable> ToList(this DataTableCollection tables)
        {
            return tables.OfType<DataTable>().ToList();
        }

        #endregion
    }
}