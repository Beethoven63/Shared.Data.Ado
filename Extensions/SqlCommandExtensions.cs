using Shared.Data.Ado.Parameter;
using Shared.Serialization;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Shared.Data.Ado.Extensions
{
    /// <summary></summary>
    public static class SqlCommandExtensions
    {
        #region Methods

        /// <summary>Adds the parameters.</summary>
        /// <param name="cmd">The command.</param>
        /// <param name="parameters">The parameters.</param>
        public static void AddParameters(this SqlCommand cmd, List<SqlParameter> parameters)
        {
            if(!parameters.Any())
                return;

            var cloner = new DeepCloner();

            foreach(var item in parameters)
            {
                var copy = cloner.DeepClone(item);
                cmd.Parameters.Add(copy);
            }
        }

        /// <summary>Adds the parameters old.</summary>
        /// <param name="cmd">The command.</param>
        /// <param name="source">The source.</param>
        public static void AddParameters_Old(this SqlCommand cmd, IParameterSource source)
        {
            if(!source.Parameters.Any())
                return;

            var cloner = new DeepCloner();

            foreach(var item in source.Parameters)
            {
                var copy = cloner.DeepClone(item);
                cmd.Parameters.Add(copy);
            }

            //var copy = source.Parameters.DeepClone().ToArray();
        }

        #endregion
    }
}