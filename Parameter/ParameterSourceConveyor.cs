using Shared.Collection;
using Shared.Data.Ado.AdoConveyor.Change;
using Shared.Data.Ado.AdoConveyor.Conveyor;
using Shared.Logging.Responses;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Data.Ado.Parameter
{
    /// <summary></summary>
    public class ParameterSourceConveyor: IDataConveyor<ParameterSourceKey, IParameterSource>
    {
        #region Fields

        /// <summary>The parameter sources</summary>
        private readonly Dictionary<ParameterSourceKey, IParameterSource> _paramSources =
                new Dictionary<ParameterSourceKey, IParameterSource>();

        #endregion

        #region Methods

        #region IDataConveyor<ParameterSourceKey,IParameterSource> Members

        /// <summary>Deletes the data.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<ParameterSourceKey> DeleteData(IDataChangeRequest<ParameterSourceKey, IParameterSource> request)
        {
            _paramSources[request.Param] = null;

            return new BaseResponse<ParameterSourceKey> {Data = request.Param, Success = !_paramSources.Any()};
        }

        /// <summary>Gets the item.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<IParameterSource> GetItem(IDataChangeRequest<ParameterSourceKey, IParameterSource> request)
        {
            if(!_paramSources.ContainsKey(request.Param))
                return new BaseResponse<IParameterSource>();

            var source = _paramSources[request.Param];

            return new BaseResponse<IParameterSource> {Data = source, Success = source != null};
        }

        /// <summary>Gets the list.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<List<IParameterSource>> GetList(IDataChangeRequest<ParameterSourceKey, IParameterSource> request)
        {
            if(!_paramSources.ContainsKey(request.Param))
                return new BaseResponse<List<IParameterSource>>();

            var source = _paramSources[request.Param];

            return new BaseResponse<List<IParameterSource>> {Data = new List<IParameterSource> {source}, Success = !source.Parameters.IsNullOrEmpty()};
        }

        /// <summary>Saves the data.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<ParameterSourceKey> SaveData(IDataChangeRequest<ParameterSourceKey, IParameterSource> request)
        {
            _paramSources[request.Param] = request.Data;

            return new BaseResponse<ParameterSourceKey> {Data = request.Param, Success = true};
        }

        #endregion

        #endregion
    }
}