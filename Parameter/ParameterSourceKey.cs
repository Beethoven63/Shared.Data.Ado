namespace Shared.Data.Ado.Parameter
{
    /// <summary></summary>
    public class ParameterSourceKey
    {
        #region Properties

        /// <summary>Gets or sets the key.</summary>
        /// <value>The key.</value>
        public string Key{get; set;}

        #endregion
    }
}