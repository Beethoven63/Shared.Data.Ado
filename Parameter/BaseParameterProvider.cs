using Shared.Strings;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Shared.Data.Ado.Parameter
{
    /// <summary></summary>
    public class BaseParameterProvider
    {
        #region Methods

        /// <summary>Creates the SQL parameter.</summary>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="sqlDbType"><see cref="Type"/> of the SQL database.</param>
        /// <param name="valuePassed">The value passed.</param>
        /// <returns></returns>
        public SqlParameter CreateSqlParameter(string parameterName, SqlDbType sqlDbType, object valuePassed)
        {
            var stringType = sqlDbType   == SqlDbType.VarChar || sqlDbType == SqlDbType.NChar;
            var nullPassed = valuePassed == null              || string.IsNullOrEmpty(valuePassed.ToString());

            if(!stringType)
                return new SqlParameter(parameterName, sqlDbType) {Value = valuePassed ?? DBNull.Value};

            valuePassed = nullPassed ? null : valuePassed.ToString().Clean("'").Trim();

            return new SqlParameter(parameterName, sqlDbType) {Value = valuePassed ?? DBNull.Value};
        }

        #endregion
    }
}