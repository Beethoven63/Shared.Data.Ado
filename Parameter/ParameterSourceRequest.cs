using Shared.Data.Ado.AdoConveyor.Change;
using Shared.Data.Ado.Paging;
using System;

namespace Shared.Data.Ado.Parameter
{
    /// <summary></summary>
    internal class ParameterSourceRequest: IDataChangeRequest<ParameterSourceKey, IParameterSource>
    {
        #region Properties

        /// <summary>Gets or sets the data.</summary>
        /// <value>The data.</value>
        public IParameterSource Data{get; set;}

        /// <summary>Gets or sets the name.</summary>
        /// <value>The name.</value>
        public string Name{get; set;}

        /// <summary>Gets or sets the page.</summary>
        /// <value>The page.</value>
        public PageParam Page{get; set;}

        /// <summary>Gets or sets the parameter.</summary>
        /// <value>The parameter.</value>
        public ParameterSourceKey Param{get; set;}

        /// <summary>Gets or sets the type.</summary>
        /// <value>The type.</value>
        public Type Type{get; set;}

        #endregion
    }
}