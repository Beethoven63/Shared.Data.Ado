using System.Collections.Generic;
using System.Data.SqlClient;

namespace Shared.Data.Ado.Parameter
{
    /// <summary></summary>
    public interface IParameterSource
    {
        #region Properties

        /// <summary>Gets or sets the number of parameters expected.</summary>
        /// <value>The number of parameters expected.</value>
        int NumberOfParametersExpected{get; set;}

        /// <summary>Gets or sets the parameters.</summary>
        /// <value>The parameters.</value>
        List<SqlParameter> Parameters{get; set;}

        /// <summary>Gets or sets the source key.</summary>
        /// <value>The source key.</value>
        string SourceKey{get; set;}

        #endregion
    }
}