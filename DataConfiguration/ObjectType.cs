namespace Shared.Data.Ado.DataConfiguration
{
    /// <summary></summary>
    public enum ObjectType
    {
        /// <summary>The table</summary>
        Table,

        /// <summary>The stored procedure</summary>
        StoredProcedure,

        /// <summary>The view</summary>
        View
    }
}