using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Shared.Data.Ado.DataConfiguration
{
    /// <summary></summary>
    public sealed class ServerElement: ConfigurationElement
    {
        #region Properties

        /// <summary>Gets the database list.</summary>
        /// <value>The database list.</value>
        public List<DatabaseElement> DatabaseList => Databases.ToList();

        /// <summary>Gets the databases.</summary>
        /// <value>The databases.</value>
        [ConfigurationProperty("Databases", IsDefaultCollection = true)]
        public DatabaseElementCollection Databases => (DatabaseElementCollection)base["Databases"];

        /// <summary>Gets or sets the server address.</summary>
        /// <value>The server address.</value>
        [ConfigurationProperty("ServerAddress", IsRequired = true)]
        public string ServerAddress
        {
            get => (string)this["ServerAddress"];

            set => this["ServerAddress"] = value;
        }

        /// <summary>Gets or sets the name of the server.</summary>
        /// <value>The name of the server.</value>
        [ConfigurationProperty("ServerName", IsKey = true, IsRequired = true)]
        public string ServerName
        {
            get => (string)this["ServerName"];

            set => this["ServerName"] = value;
        }

        #endregion

        #region Methods

        /// <summary>Converts to string.</summary>
        /// <returns>A <see cref="System.String"/> that represents this instance.</returns>
        public override string ToString()
        {
            return ServerName;
        }

        #endregion
    }
}