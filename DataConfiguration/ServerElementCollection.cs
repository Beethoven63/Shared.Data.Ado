using Shared.FunctionalPattern;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Shared.Data.Ado.DataConfiguration
{
    /// <summary></summary>
    [ConfigurationCollection(typeof(ServerElement), AddItemName = "add",
                             CollectionType                     = ConfigurationElementCollectionType.AddRemoveClearMap)]
    public class ServerElementCollection: ConfigurationElementCollection, IEnumerable<ServerElement>
    {
        #region Fields

        private readonly Guardian _guard = new Guardian();

        #endregion

        #region Properties

        /// <summary>Gets or sets the <see cref="ServerElement"/> at the specified index.</summary>
        /// <value>The <see cref="ServerElement"/> .</value>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public ServerElement this[int index]
        {
            get => (ServerElement)BaseGet(index);

            set
            {
                if(BaseGet(index) != null)
                    BaseRemoveAt(index);

                BaseAdd(index, value);
            }
        }

        /// <summary>Gets the <see cref="ServerElement"/> with the specified title.</summary>
        /// <value>The <see cref="ServerElement"/> .</value>
        /// <param name="title">The title.</param>
        /// <returns></returns>
        public new ServerElement this[string title] => (ServerElement)BaseGet(title);

        #endregion

        #region Methods

        /// <summary>Determines whether the specified <paramref name="key"/> contains key.</summary>
        /// <param name="key">The key.</param>
        /// <returns><c>true</c> if the specified <paramref name="key"/> contains key; otherwise, <c>false</c> .</returns>
        public bool ContainsKey(string key)
        {
            var keys = BaseGetAllKeys();

            return keys.Any(obj => obj.ToString() == key);
        }

        /// <summary>Returns an enumerator that iterates through the collection.</summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        public new IEnumerator<ServerElement> GetEnumerator()
        {
            return BaseGetAllKeys().Select(key => (ServerElement)BaseGet(key)).GetEnumerator();
        }

        /// <summary>When overridden in a derived class, creates a new <see cref="ConfigurationElement"/> .</summary>
        /// <returns>A newly created <see cref="ConfigurationElement"/> .</returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new ServerElement();
        }

        /// <summary>Gets the <paramref name="element"/> key for a specified configuration <paramref name="element"/> when overridden in a derived class.</summary>
        /// <param name="element">The <see cref="ConfigurationElement"/> to return the key for.</param>
        /// <exception cref="Exception">Null <paramref name="element"/></exception>
        /// <returns>An <see cref="Object"/> that acts as the key for the specified <see cref="ConfigurationElement"/> .</returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            var elem = element as ServerElement;
            _guard.CheckObject(elem);

            if(elem == null)
                throw new Exception("Null element");

            return elem.ServerName;
        }

        #endregion
    }
}