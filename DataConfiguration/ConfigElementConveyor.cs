using Shared.Configuration;
using Shared.ConsoleHelper;
using Shared.Data.Ado.AdoConveyor.Change;
using Shared.Data.Ado.AdoConveyor.Conveyor;
using Shared.Data.Ado.AdoConveyor.Query;
using Shared.Logging.MainLogging;
using Shared.Logging.Responses;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Shared.Data.Ado.DataConfiguration
{
    /// <summary></summary>
    /// <typeparam name="TSection">The type of the section.</typeparam>
    /// <typeparam name="TElement">The type of the element.</typeparam>
    public class ConfigElementConveyor<TSection, TElement>: IDataConveyor<string, TElement>
            where TSection: ConfigurationSectionWrapper, new()
            where TElement: ConfigurationElementWrapper, new()
    {
        #region Fields

        /// <summary>The current configuration</summary>
        private System.Configuration.Configuration _currentConfig;

        /// <summary>The current section</summary>
        private TSection _currentSection;

        #endregion

        #region Methods

        #region IDataConveyor<string,TElement> Members

        /// <summary>Deletes the data.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<string> DeleteData(IDataChangeRequest<string, TElement> request)
        {
            // Get the current configuration file.
            GetConfig();

            GetSection();

            if(!CheckForExistingSection())
                return new BaseResponse<string> {Success = false};

            // Use one of the ConfigurationCollectionElement Remove overloaded methods to remove the
            // element from the collection.
            _currentSection.Elements.Remove(request.Data);

            Save(ConfigurationSaveMode.Full);

            var message = $"Removed collection element from he custom section in the configuration file: {_currentConfig.FilePath}";

            SbLog.It.Message(LogSeverity.Trace, message);

            return new BaseResponse<string>();
        }

        /// <summary>Gets the item.</summary>
        /// <param name="request">The request.</param>
        /// <exception cref="NotImplementedException"/>
        /// <returns></returns>
        public BaseResponse<TElement> GetItem(IDataChangeRequest<string, TElement> request)
        {
            throw new NotImplementedException();
        }

        /// <summary>Gets the list.</summary>
        /// <param name="request">The request.</param>
        /// <exception cref="Exception">Failed to load UrlsSection.</exception>
        /// <returns></returns>
        public BaseResponse<List<TElement>> GetList(IDataChangeRequest<string, TElement> request)
        {
            // Get the application configuration file.
            GetConfig();

            // Read and display the custom section.
            GetSection();

            if(_currentSection == null)
                throw new Exception("Failed to load UrlsSection.");

            return new BaseResponse<List<TElement>> {Data = _currentSection.Elements.Select(e => (TElement)e).ToList(), Success = true};
        }

        /// <summary>Saves the data.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BaseResponse<string> SaveData(IDataChangeRequest<string, TElement> request)
        {
            GetConfig();

            // Get the custom configuration section.
            GetSection();

            // Add the element to the collection in the custom section.
            if(!CheckForExistingSection())
                return new BaseResponse<string> {Success = false};

            // Use the ConfigurationCollectionElement Add method to add the new element to the collection.
            _currentSection.Elements.Add(request.Data);

            Save(ConfigurationSaveMode.Modified);

            var message = $"Added collection element to the custom section in the configuration file: {_currentConfig.FilePath}";

            SbLog.It.Message(LogSeverity.Trace, message);

            return new BaseResponse<string>();
        }

        #endregion

        /// <summary>Checks for existing section.</summary>
        /// <returns></returns>
        public bool CheckForExistingSection()
        {
            if(_currentConfig.Sections["MyUrls"] != null)
                return true;

            SbLog.It.Message(LogSeverity.Warn, "You must create the custom section first.");

            return false;
        }

        // Add an element to the custom section collection. This function uses the
        // ConfigurationCollectionElement Add method. Remove the collection of elements from the
        // custom section. This function uses the ConfigurationCollectionElement Clear method.
        /// <summary>Clears the collection elements.</summary>
        public void ClearCollectionElements()
        {
            // Get the current configuration file.
            GetConfig();

            // Get the custom configuration section.
            GetSection();

            // Remove the collection of elements from the section. Add the element to the collection
            // in the custom section.
            if(!CheckForExistingSection())
                return;

            _currentSection.Elements.Clear();

            // Save the application configuration file.
            Save(ConfigurationSaveMode.Full);

            var message = $"Removed collection of elements from he custom section in the configuration file: {_currentConfig.FilePath}";

            SbLog.It.Message(LogSeverity.Trace, message);
        }

        // Create a custom section and save it in the application configuration file.
        /// <summary>Creates the custom section.</summary>
        public void CreateCustomSection()
        {
            // Get the current configuration file.
            GetConfig();

            // Add the custom section to the application configuration file.
            GetSection();

            if(_currentSection == null)
            {
                // The configuration file does not contain the custom section yet. Create it.
                _currentSection = new TSection();

                _currentConfig.Sections.Add("MyUrls", _currentSection);
            }
            else if(_currentSection.Elements.Count == 0)
            {
                // The configuration file contains the custom section but its element collection is
                // empty. Initialize the collection.
                _currentSection.Elements.Add(new TElement());
            }

            // Save the application configuration file.
            Save(ConfigurationSaveMode.Modified);

            var message = $"Created custom section in the application configuration file: {_currentConfig.FilePath}";

            SbLog.It.Message(LogSeverity.Trace, message);
        }

        /// <summary>Gets the list.</summary>
        /// <param name="request">The request.</param>
        /// <param name="procedure">The procedure.</param>
        /// <exception cref="NotImplementedException"/>
        /// <returns></returns>
        public BaseResponse<List<TElement>> GetList(IDataChangeRequest<string, TElement> request,
                                                    IStoredProcedure<string, TElement> procedure)
        {
            throw new NotImplementedException();
        }

        /// <summary>Reads the custom section.</summary>
        public void ReadCustomSection() {}

        // Remove element from the custom section collection. This function uses one of the
        // ConfigurationCollectionElement overloaded Remove methods.
        /// <summary>Removes the collection element.</summary>
        public void RemoveCollectionElement() {}

        /// <summary>Stubs the action.</summary>
        /// <param name="arg">The argument.</param>
        /// <returns></returns>
        public BaseResponse<ConfigResponse> StubAction(ConsoleRequest arg)
        {
            return new BaseResponse<ConfigResponse> {Success = true};
        }

        /// <summary>Gets the configuration.</summary>
        private void GetConfig()
        {
            // Get the current configuration file.
            _currentConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        }

        /// <summary>Gets the section.</summary>
        private void GetSection()
        {
            // Get the custom configuration section.
            _currentSection = (TSection)_currentConfig.GetSection("MyUrls");
        }

        /// <summary>Saves the specified mode.</summary>
        /// <param name="mode">The mode.</param>
        private void Save(ConfigurationSaveMode mode)
        {
            // Save the application configuration file.
            _currentSection.SectionInformation.ForceSave = true;
            _currentConfig.Save(mode);
        }

        #endregion
    }
}