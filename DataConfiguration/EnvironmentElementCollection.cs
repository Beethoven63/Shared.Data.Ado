using Shared.FunctionalPattern;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Shared.Data.Ado.DataConfiguration
{
    /// <summary></summary>
    [ConfigurationCollection(typeof(EnvironmentElement), AddItemName = "add",
                             CollectionType =
                                     ConfigurationElementCollectionType.AddRemoveClearMap)]
    public class EnvironmentElementCollection: ConfigurationElementCollection, IEnumerable<EnvironmentElement>
    {
        #region Fields

        /// <summary>The guard</summary>
        private readonly Guardian _guard = new Guardian();

        #endregion

        #region Properties

        /// <summary>Gets or sets the <see cref="EnvironmentElement"/> at the specified index.</summary>
        /// <value>The <see cref="EnvironmentElement"/> .</value>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public EnvironmentElement this[int index]
        {
            get => (EnvironmentElement)BaseGet(index);

            set
            {
                if(BaseGet(index) != null)
                    BaseRemoveAt(index);

                BaseAdd(index, value);
            }
        }

        /// <summary>Gets the <see cref="EnvironmentElement"/> with the specified title.</summary>
        /// <value>The <see cref="EnvironmentElement"/> .</value>
        /// <param name="title">The title.</param>
        /// <returns></returns>
        public new EnvironmentElement this[string title] => (EnvironmentElement)BaseGet(title);

        #endregion

        #region Methods

        /// <summary>Returns an enumerator that iterates through the collection.</summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        public new IEnumerator<EnvironmentElement> GetEnumerator()
        {
            return BaseGetAllKeys().Select(key => (EnvironmentElement)BaseGet(key)).GetEnumerator();
        }

        /// <summary>When overridden in a derived class, creates a new <see cref="ConfigurationElement"/> .</summary>
        /// <returns>A newly created <see cref="ConfigurationElement"/> .</returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new EnvironmentElement();
        }

        /// <summary>Gets the <paramref name="element"/> key for a specified configuration <paramref name="element"/> when overridden in a derived class.</summary>
        /// <param name="element">The <see cref="ConfigurationElement"/> to return the key for.</param>
        /// <exception cref="Exception">Null <paramref name="element"/></exception>
        /// <returns>An <see cref="Object"/> that acts as the key for the specified <see cref="ConfigurationElement"/> .</returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            var elem = element as EnvironmentElement;
            _guard.CheckObject(elem);

            if(elem == null)
                throw new Exception("Null element");

            return elem.EnvName;
        }

        #endregion
    }
}