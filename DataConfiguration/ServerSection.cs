using System.Configuration;

namespace Shared.Data.Ado.DataConfiguration
{
    /// <summary></summary>
    /// <seealso cref="T:System.Configuration.ConfigurationSection"/>
    public class ServerSection: ConfigurationSection
    {
        #region Properties

        /// <summary>Gets the servers.</summary>
        /// <value>The servers.</value>
        [ConfigurationProperty("Servers", IsRequired = true)]
        [ConfigurationCollection(typeof(ServerElement), AddItemName = "add", ClearItemsName = "clear",
                                 RemoveItemName                     = "remove")]
        public ConfigElementCollection<ServerElement> Servers =>
                (ConfigElementCollection<ServerElement>)this["Servers"];

        #endregion
    }
}