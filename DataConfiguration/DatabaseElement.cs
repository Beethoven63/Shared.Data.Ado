using System.Configuration;

namespace Shared.Data.Ado.DataConfiguration
{
    /// <summary></summary>
    public sealed class DatabaseElement: ConfigurationElement
    {
        #region Properties

        /// <summary>Gets or sets the connection key.</summary>
        /// <value>The connection key.</value>
        [ConfigurationProperty("ConnectionKey", IsRequired = true)]
        public string ConnectionKey
        {
            get => (string)this["ConnectionKey"];

            set => this["ConnectionKey"] = value;
        }

        /// <summary>Gets the data objects.</summary>
        /// <value>The data objects.</value>
        [ConfigurationProperty("DataObjects", IsDefaultCollection = true)]
        public DataObjectElementCollection DataObjects => (DataObjectElementCollection)base["DataObjects"];

        /// <summary>Gets or sets the name of the database.</summary>
        /// <value>The name of the database.</value>
        [ConfigurationProperty("DBName", IsKey = true, IsRequired = true)]
        public string DbName
        {
            get => (string)this["DBName"];

            set => this["DBName"] = value;
        }

        #endregion

        #region Methods

        /// <summary>Converts to string.</summary>
        /// <returns>A <see cref="System.String"/> that represents this instance.</returns>
        public override string ToString()
        {
            return DbName;
        }

        #endregion
    }
}