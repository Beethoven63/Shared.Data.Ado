using Shared.FunctionalPattern;
using Shared.Logging.Responses;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Shared.Data.Ado.DataConfiguration
{
    /// <summary></summary>
    public class DatabaseConfigSearcher
    {
        #region Fields

        /// <summary>The guardian</summary>
        private readonly Guardian _guardian = new Guardian();

        #endregion

        #region Methods

        /// <summary>Finds the matching database.</summary>
        /// <param name="tableAddress">The table address.</param>
        /// <exception cref="Exception">Database {tableAddress.Database} not found on server {tableAddress.Server}</exception>
        /// <returns></returns>
        public DatabaseElement FindMatchingDatabase(DataObjectAddress tableAddress)
        {
            var server = GetServer(tableAddress.Environment, tableAddress.Server);

            var list = (IEnumerable<DatabaseElement>)server.Databases;

            var db = list.FirstOrDefault(d => d.DbName == tableAddress.Database);

            if(db == null)
                throw new Exception($"Database {tableAddress.Database} not found on server {tableAddress.Server}");

            return db;
        }

        /// <summary>Gets the data base.</summary>
        /// <param name="address">The address.</param>
        /// <returns></returns>
        public BaseResponse<DatabaseElement> GetDataBase(DataObjectAddress address)
        {
            _guardian.CheckObject(address);

            if(!EnvironmentSectionExists())
                return new BaseResponse<DatabaseElement>();

            var database = GetDatabase(address.Environment, address.Server, address.Database);

            return new BaseResponse<DatabaseElement> {Success = database != null, Data = database};
        }

        /// <summary>Gets the environment.</summary>
        /// <param name="environmentName">Name of the environment.</param>
        /// <returns></returns>
        public EnvironmentElement GetEnvironment(string environmentName)
        {
            var section = ConfigurationManager.GetSection("EnvironmentSection") as EnvironmentSection;
            _guardian.CheckObject(section);

            // ReSharper disable once PossibleNullReferenceException
            var env = section?.Environments.FirstOrDefault(e => e.EnvName == environmentName);
            _guardian.CheckObject(env);

            return env;
        }

        /// <summary>Gets the server.</summary>
        /// <param name="environmentName">Name of the environment.</param>
        /// <param name="serverName">Name of the server.</param>
        /// <returns></returns>
        public ServerElement GetServer(string environmentName, string serverName)
        {
            var environment = GetEnvironment(environmentName);
            _guardian.CheckObject(environment);

            var server = environment.Servers.FirstOrDefault(s => s.ServerName == serverName);
            _guardian.CheckObject(server);

            return server;
        }

        /// <summary>Environments the section exists.</summary>
        /// <exception cref="Exception"><see cref="Environment"/> section is missing</exception>
        /// <returns></returns>
        private static bool EnvironmentSectionExists()
        {
            return ConfigurationManager.GetSection("EnvironmentSection") is EnvironmentSection
                           ? true
                           : throw new Exception("Environment section is missing");
        }

        /// <summary>Gets the database.</summary>
        /// <param name="environmentName">Name of the environment.</param>
        /// <param name="serverName">Name of the server.</param>
        /// <param name="databaseName">Name of the database.</param>
        /// <returns></returns>
        private DatabaseElement GetDatabase(string environmentName, string serverName, string databaseName)
        {
            var server = GetServer(environmentName, serverName);
            _guardian.CheckObject(server);

            var database = server.DatabaseList.FirstOrDefault(d => d.DbName == databaseName);
            _guardian.CheckObject(database);

            return database;
        }

        public DataObjectElement GetDataObject(string environmentName, string serverName, string databaseName,
                                               string tableName)
        {
            var database = GetDatabase(environmentName, serverName, databaseName);
            _guardian.CheckObject(database);

            var table = database.DataObjects.FirstOrDefault(d => d.ObjName == tableName);
            _guardian.CheckObject(table);

            return table;
        }

        #endregion
    }
}

//private List<DataObjectElement> GetDataObjects()
//{
////var section = ConfigurationManager.GetSection("EnvironmentSection") as EnvironmentSection;
////_guardian.CheckObject(section);

////// ReSharper disable once PossibleNullReferenceException
////var servers = section.Environments.SelectMany(e => e.Servers);
////var databases = servers.SelectMany(s => s.DatabaseList);
////var dataObjects = databases.SelectMany(d => d.DataObjects);

////return dataObjects.ToList();
//return null;
//}
//public BaseResponse<DataObjectAddress> FindObjectAddress(string dataObjectName)
//{
//    //var section = ConfigurationManager.GetSection("EnvironmentSection") as EnvironmentSection;
//    //_guardian.CheckObject(section);

// //// ReSharper disable once PossibleNullReferenceException //var envs = section.Environments.ToList();

// //var servers = envs.SelectMany(e => e.Servers).ToList(); //var databases = servers.SelectMany(s
// => s.DatabaseList).ToList(); ////var dataObjects = databases.SelectMany(d => d.DataObjects).ToList();

// //var dataObject = GetDataObjects().FirstOrDefault(d => d.ObjName == dataObjectName); //if
// (dataObject == null) //{ // SbLog.It.Message(LogSeverity.Warn, "Unable to find config entry for
// data object {0}", dataObjectName); // return new BaseResponse<DataObjectAddress>(); //}

// //var database = databases.FirstOrDefault(d => d.DataObjects.Any(obj => obj.ObjName ==
// dataObject.ObjName)); //if (database == null) //{ // SbLog.It.Message(LogSeverity.Warn, "Unable
// to find config database for data object {0}", dataObject.ObjName); // return new
// BaseResponse<DataObjectAddress>(); //}

// //var server = servers.FirstOrDefault(s => s.DatabaseList.Any(obj => obj.DbName ==
// database.DbName)); //if (server == null) //{ // SbLog.It.Message(LogSeverity.Warn, "Unable to
// find config server for database {0}", database.DbName); // return new
// BaseResponse<DataObjectAddress>(); //}

// //var environment = envs.FirstOrDefault(e => e.Servers.Any(s => s.ServerName ==
// server.ServerName)); //// ReSharper disable once InvertIf //if (environment == null) //{ //
// SbLog.It.Message(LogSeverity.Warn, "Unable to find config environment for server {0}",
// server.ServerName); // return new BaseResponse<DataObjectAddress>(); //}

//    //return new BaseResponse<DataObjectAddress>
//    //{
//    //    Data = new DataObjectAddress
//    //    {
//    //        Environment = environment.EnvName,
//    //        Server = server.ServerName,
//    //        Schema = dataObject.Schema,
//    //        Database = database.DbName,
//    //        DbObject = dataObject.ObjName
//    //    },
//    //    Success = true
//    //};
//    return null;
//}