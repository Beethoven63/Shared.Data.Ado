using System.Configuration;

namespace Shared.Data.Ado.DataConfiguration
{
    /// <summary></summary>
    public class EnvironmentSection: ConfigurationSection
    {
        #region Properties

        /// <summary>Gets the environments.</summary>
        /// <value>The environments.</value>
        [ConfigurationProperty("Environments", IsRequired = true)]
        [ConfigurationCollection(typeof(EnvironmentElement), AddItemName = "add", ClearItemsName = "clear",
                                 RemoveItemName                          = "remove")]
        public EnvironmentElementCollection Environments => (EnvironmentElementCollection)this["Environments"];

        #endregion
    }
}