using System.Configuration;

namespace Shared.Data.Ado.DataConfiguration
{
    /// <summary></summary>
    public sealed class EnvironmentElement: ConfigurationElement
    {
        #region Properties

        /// <summary>Gets or sets the code path.</summary>
        /// <value>The code path.</value>
        [ConfigurationProperty("CodePath", IsRequired = true)]
        public string CodePath
        {
            get => (string)this["CodePath"];

            set => this["CodePath"] = value;
        }

        /// <summary>Gets or sets the name of the env.</summary>
        /// <value>The name of the env.</value>
        [ConfigurationProperty("EnvName", IsKey = true, IsRequired = true)]
        public string EnvName
        {
            get => (string)this["EnvName"];

            set => this["EnvName"] = value;
        }

        /// <summary>Gets the servers.</summary>
        /// <value>The servers.</value>
        [ConfigurationProperty("Servers", IsDefaultCollection = true)]
        public ServerElementCollection Servers => (ServerElementCollection)base["Servers"];

        #endregion

        #region Methods

        /// <summary>Converts to string.</summary>
        /// <returns>A <see cref="System.String"/> that represents this instance.</returns>
        public override string ToString()
        {
            return EnvName;
        }

        #endregion
    }
}