using Shared.Collection;
using Shared.FunctionalPattern;
using Shared.Strings;
using System;
using System.Collections.Generic;

namespace Shared.Data.Ado.DataConfiguration
{
    /// <summary></summary>
    public class DataObjectAddress
    {
        #region Fields

        /// <summary>The guardian</summary>
        private readonly Guardian _guardian = new Guardian();

        #endregion

        #region Properties

        /// <summary>Gets or sets the database.</summary>
        /// <value>The database.</value>
        public string Database{get; set;}

        /// <summary>Gets or sets the database object.</summary>
        /// <value>The database object.</value>
        public string DbObject{get; set;}

        /// <summary>Gets or sets the environment.</summary>
        /// <value>The environment.</value>
        public string Environment{get; set;}

        /// <summary>Gets or sets the schema.</summary>
        /// <value>The schema.</value>
        public string Schema{get; set;}

        /// <summary>Gets or sets the server.</summary>
        /// <value>The server.</value>
        public string Server{get; set;}

        /// <summary>Gets or sets the type.</summary>
        /// <value>The type.</value>
        public ObjectType Type{get; set;}

        #endregion

        #region Methods

        /// <summary>Clones this instance.</summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public object Clone()
        {
            throw new NotImplementedException();
        }

        /// <summary>Compares to.</summary>
        /// <param name="other">The other.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public int CompareTo(DataObjectAddress other)
        {
            throw new NotImplementedException();
        }

        /// <summary>Equalses the specified other.</summary>
        /// <param name="other">The other.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool Equals(DataObjectAddress other)
        {
            throw new NotImplementedException();
        }

        /// <summary>Parses the specified address.</summary>
        /// <param name="address">The address.</param>
        public void Parse(string address)
        {
            _guardian.CheckObject(address);

            var addressParts = address.Parse("|");

            Type        = addressParts[index: 0].ToEnum<ObjectType>();
            Environment = addressParts[index: 1];
            Server      = addressParts[index: 2];
            Database    = addressParts[index: 3];
            Schema      = addressParts[index: 4];
            DbObject    = addressParts[index: 5];
        }

        /// <summary>Returns a <see cref="string"/> that represents this instance.</summary>
        /// <returns>A <see cref="string"/> that represents this instance.</returns>
        public override string ToString()
        {
            var strFields = new List<string>
            {
                Type.ToString(),
                Environment,
                Server,
                Database,
                Schema,
                DbObject
            };

            return strFields.ToDsv("|");
        }

        #endregion
    }
}