using System.Configuration;

namespace Shared.Data.Ado.DataConfiguration
{
    /// <summary></summary>
    public class DataObjectElement: ConfigurationElement
    {
        #region Properties

        /// <summary>Gets or sets the changed date.</summary>
        /// <value>The changed date.</value>
        [ConfigurationProperty("ChangedDate", DefaultValue = "MODIFICATION_DATE")]
        public string ChangedDate
        {
            get => (string)this["ChangedDate"];
            set => this["ChangedDate"] = value;
        }

        /// <summary>Gets or sets the changed user.</summary>
        /// <value>The changed user.</value>
        [ConfigurationProperty("ChangedUser", DefaultValue = "MODIFICATION_USER")]
        public string ChangedUser
        {
            get => (string)this["ChangedUser"];
            set => this["ChangedUser"] = value;
        }

        /// <summary>Gets or sets the created date.</summary>
        /// <value>The created date.</value>
        [ConfigurationProperty("CreatedDate", DefaultValue = "CREATION_DATE")]
        public string CreatedDate
        {
            get => (string)this["CreatedDate"];
            set => this["CreatedDate"] = value;
        }

        /// <summary>Gets or sets the created user.</summary>
        /// <value>The created user.</value>
        [ConfigurationProperty("CreatedUser", DefaultValue = "CREATION_USER")]
        public string CreatedUser
        {
            get => (string)this["CreatedUser"];
            set => this["CreatedUser"] = value;
        }

        /// <summary>Gets or sets the name of the object.</summary>
        /// <value>The name of the object.</value>
        [ConfigurationProperty("ObjName", IsKey = true, IsRequired = true)]
        public string ObjName
        {
            get => (string)this["ObjName"];
            set => this["ObjName"] = value;
        }

        /// <summary>Gets or sets the schema.</summary>
        /// <value>The schema.</value>
        [ConfigurationProperty("Schema", IsRequired = false, DefaultValue = "dbo")]
        public string Schema
        {
            get => (string)this["Schema"];
            set => this["Schema"] = value;
        }

        /// <summary>Gets or sets the type.</summary>
        /// <value>The type.</value>
        [ConfigurationProperty("Type", DefaultValue = "Table")]
        public string Type
        {
            get => (string)this["Type"];
            set => this["Type"] = value;
        }

        #endregion

        #region Methods

        /// <summary>Converts to string.</summary>
        /// <returns>A <see cref="System.String"/> that represents this instance.</returns>
        public override string ToString()
        {
            return ObjName;
        }

        #endregion
    }
}